<?php

namespace App\Processor\Provider\Opta;

use App\Processor\Provider\Opta\Traits\FormationPlaceTrait;
use AsResultados\OAMBundle\Exception\ValidationItemException;
use AsResultados\OAMBundle\Model\Results\Match\Embed\Goal;
use AsResultados\OAMBundle\Exception\MissingItemException;
use App\Processor\Provider\AbstractProcessor as AbstractProcessorProvider;
use App\Utils\NumberTrait;
use Exception;

abstract class AbstractProcessor extends AbstractProcessorProvider implements ProcessorInterface
{
    use NumberTrait;
    use FormationPlaceTrait;

    const PARATARTA = 'aaaa';

    /**
     * @param string $competitionId
     * @param string $seasonId
     * @return string
     * @throws Exception
     */
    protected function generateProviderCompetitionSeasonId(string $competitionId, string $seasonId): string
    {
        echo "hola hugo";
        if (empty($competitionId)) {
            throw new Exception('Competition id is empty');
        }
        if (empty($seasonId)) {
            throw new Exception('Season id is empty');
        }
        return $competitionId . '_' . $seasonId;
    }

    /**
     * @param string $competitionSeasonId
     * @param string $roundType
     * @param int $roundNumber
     * @return string
     * @throws Exception
     */
    protected function generateProviderStageId(string $competitionSeasonId, ?string $roundType, ?int $roundNumber): string
    {
        if (empty($competitionSeasonId)) {
            throw new Exception('Competition season id is empty');
        }
        $roundType = strtolower($roundType);
        $roundNumber = is_null($roundNumber) ? 0 : $roundNumber;
        switch ($roundType) {
            case 'final':
                $id = '1';
                break;
            case '3rd and 4th place':
                $id = '2';
                break;
            case "semi-finals":
                $id = '3';
                break;
            case 'quarter-finals':
                $id = '4';
                break;
            case 'round of 16':
                $id = '5';
                break;
            case "round of 32":
                $id = '6';
                break;
            case 'play-offs':
                $id = '7';
                break;
            case 'regular':
            case 'round':
            case '':
                $id = '10';
                break;
            case 'qualifier round':
                $id = '11';
                break;
            default:
                throw new Exception("Unknown stage type: " . $roundType);
        }
        $result = $competitionSeasonId . '_' . $id . 'n' . $roundNumber;
        return $result;
    }

    /**
     * @param string $providerGroup
     * @return int
     * @throws Exception
     */
    protected function generateGroupNumber(string $providerGroup): int
    {
        return is_numeric($providerGroup) ?
            (int)$providerGroup :
            $this->letterToNumber($providerGroup);
    }

    /**
     * @param string $providerLeg
     * @return int
     * @throws MissingItemException
     */
    protected function generateLegNumber(string $providerLeg): int
    {
        switch ($providerLeg) {
            case '1st Leg':
                return 1;
            case '2nd Leg':
                return 2;
        }
        throw new MissingItemException($providerLeg, 'Provider Leg', 'method: ' . __METHOD__);
    }

    /**
     * @param Goal $goal
     * @param string $type
     * @return void
     */
    protected function setGoalType(Goal &$goal, string $type): void
    {
        $goal->setOwnGoal(false);
        $goal->setPenalty(false);
        $type = strtolower($type);
        switch ($type) {
            case 'own':
            case 'owng':
                $goal->setOwnGoal(true);
                break;
            case 'penalty':
            case 'peng':
                $goal->setPenalty(true);
                break;
        }
    }

    /**
     * @param string $value
     * @return string|null
     */
    protected function checkValue(string $value): ?string
    {
        $tmp = strtolower($value);
        if (!empty($value) && ($tmp !== 'unknown')) {
            return $value;
        }
        return null;
    }

    /**
     * @param int $matchDay
     * @param int|null $stageMatchDayStart
     * @return int
     * @throws ValidationItemException
     */
    protected function getRealMatchDay(int $matchDay, ?int $stageMatchDayStart): int
    {
        if (is_null($stageMatchDayStart)) {
            $stageMatchDayStart = 1;
        }
        $result = $matchDay - $stageMatchDayStart + 1;
        if ($result <= 0) {
            throw new ValidationItemException($result, 'MatchDay should be greater than 0', __METHOD__);
        }
        return $result;
    }
}