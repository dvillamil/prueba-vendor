<?php

namespace App\Processor\Provider\Opta;

interface ProcessorInterface
{
    public const PROVIDER = 'OPTA2';

    public const DATETIME_ZONE = 'Europe/London';
}