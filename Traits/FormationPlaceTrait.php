<?php

namespace App\Processor\Provider\Opta\Traits;

use AsResultados\OAMBundle\Exception\MissingItemException;
use AsResultados\OAMBundle\Exception\ValidationItemException;
use AsResultados\OAMBundle\Model\Results\Category\Category;

trait FormationPlaceTrait
{
    /**
     * @param int $providerPositionId
     * @param Category $formation
     * @return int
     * @throws MissingItemException
     * @throws ValidationItemException
     */
    public function getFormationPlace(int $providerPositionId, Category $formation): int
    {
        $method = 'getFormationPlace_' . $formation->getId();
        if (!method_exists($this, $method)) {
            throw new MissingItemException($formation->getId(), 'formation', self::class);
        }
        return $this->$method($providerPositionId);
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_541(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 6;
            case 4:
                return 5;
            case 5:
                return 4;
            case 6:
                return 3;
            case 7:
                return 7;
            case 8:
                return 8;
            case 9:
                return 11;
            case 10:
                return 9;
            case 11:
                return 10;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_4231(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 5;
            case 4:
                return 7;
            case 5:
                return 3;
            case 6:
                return 4;
            case 7:
                return 8;
            case 8:
                return 6;
            case 9:
                return 11;
            case 10:
                return 9;
            case 11:
                return 10;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_442(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 5;
            case 4:
                return 6;
            case 5:
                return 3;
            case 6:
                return 4;
            case 7:
                return 8;
            case 8:
                return 7;
            case 9:
                return 11;
            case 10:
                return 10;
            case 11:
                return 9;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_41212(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 5;
            case 4:
                return 7;
            case 5:
                return 3;
            case 6:
                return 4;
            case 7:
                return 6;
            case 8:
                return 9;
            case 9:
                return 11;
            case 10:
                return 10;
            case 11:
                return 8;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_34213(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 5;
            case 3:
                return 8;
            case 4:
                return 4;
            case 5:
                return 3;
            case 6:
                return 2;
            case 7:
                return 6;
            case 8:
                return 7;
            case 9:
                return 10;
            case 10:
                return 9;
            case 11:
                return 11;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_532(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 6;
            case 4:
                return 5;
            case 5:
                return 4;
            case 6:
                return 3;
            case 7:
                return 7;
            case 8:
                return 8;
            case 9:
                return 11;
            case 10:
                return 10;
            case 11:
                return 9;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_433(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 5;
            case 4:
                return 7;
            case 5:
                return 3;
            case 6:
                return 4;
            case 7:
                return 6;
            case 8:
                return 8;
            case 9:
                return 10;
            case 10:
                return 9;
            case 11:
                return 11;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_4141(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 5;
            case 4:
                return 6;
            case 5:
                return 3;
            case 6:
                return 4;
            case 7:
                return 7;
            case 8:
                return 8;
            case 9:
                return 11;
            case 10:
                return 9;
            case 11:
                return 10;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_4411(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 5;
            case 4:
                return 7;
            case 5:
                return 3;
            case 6:
                return 4;
            case 7:
                return 6;
            case 8:
                return 8;
            case 9:
                return 11;
            case 10:
                return 10;
            case 11:
                return 9;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_31312(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 5;
            case 4:
                return 7;
            case 5:
                return 3;
            case 6:
                return 4;
            case 7:
                return 6;
            case 8:
                return 9;
            case 9:
                return 10;
            case 10:
                return 11;
            case 11:
                return 8;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_4132(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 5;
            case 4:
                return 6;
            case 5:
                return 3;
            case 6:
                return 4;
            case 7:
                return 7;
            case 8:
                return 8;
            case 9:
                return 10;
            case 10:
                return 11;
            case 11:
                return 9;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_34122(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 5;
            case 3:
                return 9;
            case 4:
                return 4;
            case 5:
                return 3;
            case 6:
                return 2;
            case 7:
                return 6;
            case 8:
                return 8;
            case 9:
                return 11;
            case 10:
                return 10;
            case 11:
                return 7;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_451(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 5;
            case 4:
                return 6;
            case 5:
                return 3;
            case 6:
                return 4;
            case 7:
                return 9;
            case 8:
                return 8;
            case 9:
                return 11;
            case 10:
                return 7;
            case 11:
                return 10;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_4321(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 5;
            case 4:
                return 7;
            case 5:
                return 3;
            case 6:
                return 4;
            case 7:
                return 8;
            case 8:
                return 6;
            case 9:
                return 11;
            case 10:
                return 9;
            case 11:
                return 10;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_341211(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 5;
            case 3:
                return 9;
            case 4:
                return 4;
            case 5:
                return 3;
            case 6:
                return 2;
            case 7:
                return 6;
            case 8:
                return 8;
            case 9:
                return 11;
            case 10:
                return 10;
            case 11:
                return 7;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_3421(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 5;
            case 3:
                return 8;
            case 4:
                return 4;
            case 5:
                return 3;
            case 6:
                return 2;
            case 7:
                return 6;
            case 8:
                return 7;
            case 9:
                return 11;
            case 10:
                return 9;
            case 11:
                return 10;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_3412(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 5;
            case 3:
                return 8;
            case 4:
                return 4;
            case 5:
                return 3;
            case 6:
                return 2;
            case 7:
                return 6;
            case 8:
                return 7;
            case 9:
                return 9;
            case 10:
                return 10;
            case 11:
                return 11;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_3331(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 6;
            case 3:
                return 8;
            case 4:
                return 4;
            case 5:
                return 3;
            case 6:
                return 2;
            case 7:
                return 10;
            case 8:
                return 7;
            case 9:
                return 9;
            case 10:
                return 5;
            case 11:
                return 11;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_3241(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 4;
            case 5:
                return 5;
            case 6:
                return 6;
            case 7:
                return 7;
            case 8:
                return 8;
            case 9:
                return 9;
            case 10:
                return 10;
            case 11:
                return 11;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_3142(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 4;
            case 5:
                return 5;
            case 6:
                return 6;
            case 7:
                return 7;
            case 8:
                return 8;
            case 9:
                return 9;
            case 10:
                return 10;
            case 11:
                return 11;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_4240(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 4;
            case 5:
                return 5;
            case 6:
                return 6;
            case 7:
                return 7;
            case 8:
                return 8;
            case 9:
                return 9;
            case 10:
                return 10;
            case 11:
                return 11;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_343_d(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 4;
            case 5:
                return 5;
            case 6:
                return 6;
            case 7:
                return 7;
            case 8:
                return 8;
            case 9:
                return 9;
            case 10:
                return 10;
            case 11:
                return 11;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_343(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 5;
            case 3:
                return 8;
            case 4:
                return 4;
            case 5:
                return 3;
            case 6:
                return 2;
            case 7:
                return 6;
            case 8:
                return 7;
            case 9:
                return 10;
            case 10:
                return 9;
            case 11:
                return 11;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_352(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 5;
            case 3:
                return 9;
            case 4:
                return 4;
            case 5:
                return 3;
            case 6:
                return 2;
            case 7:
                return 6;
            case 8:
                return 8;
            case 9:
                return 11;
            case 10:
                return 10;
            case 11:
                return 7;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_4312(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 5;
            case 4:
                return 7;
            case 5:
                return 3;
            case 6:
                return 4;
            case 7:
                return 6;
            case 8:
                return 9;
            case 9:
                return 10;
            case 10:
                return 11;
            case 11:
                return 8;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }

    /**
     * @param int $providerPositionId
     * @return int
     * @throws ValidationItemException
     */
    private function getFormationPlace_TEAM_FORMATION_4222(int $providerPositionId): int
    {
        switch ($providerPositionId) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 5;
            case 4:
                return 6;
            case 5:
                return 3;
            case 6:
                return 4;
            case 7:
                return 8;
            case 8:
                return 7;
            case 9:
                return 11;
            case 10:
                return 10;
            case 11:
                return 9;
            default:
                throw new ValidationItemException('Position not recognized: ' . $providerPositionId, 'position should be between 1 and 11', self::class);
        }
    }
}