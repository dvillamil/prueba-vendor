<?php

namespace App\Processor\Provider\Opta\Xml;

use App\Event\ResourceProcessed\Live\LiveBroadcastEvent;
use App\Processor\Provider\Traits\BroadcastTrait;
use App\Processor\Provider\Traits\LanguageIdTrait;
use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\MappingInterface;
use AsResultados\OAMBundle\Api\Internal\Results\Broadcast\Register as BroadcastRegister;
use AsResultados\OAMBundle\Exception\MissingItemException;
use AsResultados\OAMBundle\Model\Collection\Collection;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Results\Broadcast\Broadcast;
use AsResultados\OAMBundle\Model\Results\Broadcast\Embed\Comment;
use AsResultados\OAMBundle\Model\Results\Category\Category;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use AsResultados\OAMBundle\Exception\EmptyItemException;
use AsResultados\OAMBundle\Exception\MappingException;
use App\Processor\Provider\Traits\CompetitionSeasonIdTrait;
use App\Processor\Provider\Traits\MatchIdTrait;
use App\Utils\FootballTrait;
use AsResultados\OAMBundle\Model\Results\Moment;
use Exception;
use Symfony\Component\DomCrawler\Crawler;

class F13m extends AbstractProcessor
{
    use FootballTrait;
    use BroadcastTrait;
    use MatchIdTrait;
    use LanguageIdTrait;
    use CompetitionSeasonIdTrait;

    /**
     * @var string
     */
    protected $matchProviderId;

    /**
     * @var string
     */
    protected $langProviderId;

    /**
     * @inheritDoc
     */
    protected function setProcessorVariablesFromMapping(): void
    {
        $mappingCollection = MappingCollection::getInstance();
        $this->setMatchId($mappingCollection->get(
            MappingInterface::ENTITY_MATCH, self::PROVIDER, $this->getMatchIdFromXml()
        ));
        $this->setLanguageId($mappingCollection->get(
            MappingInterface::ENTITY_LANGUAGE, self::PROVIDER, $this->getLanguageIdFromXml()
        ));
        try {
            $this->setBroadcastByMatchAndLanguage($this->getMatchId(), $this->getLanguageId(), $this->getClient());
            $mappingCollection->addOwn($this->getBroadcast());
        } catch (Exception $e) {
            //New broadcast
        }
    }

    /**
     * @inheritDoc
     */
    public function run(): bool
    {
        $broadcasts = $this->getBroadcastsFromXmlNode();
        $broadcastRegister = BroadcastRegister::getInstance($this->getClient());
        //Update broadcast
        try {
            //Put because we have all data and we want to override comments array
            $broadcastRegister->put($broadcasts->getAllRegistered());
            $opUpdateBroadcast = true;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update broadcast: ' . $e->getMessage());
        }
        //Insert broadcast
        try {
            $broadcastRegister->post($broadcasts->getAllUnRegistered());
            $opInsertBroadcast = true;
            //Add new classifications to collection
            $broadcasts->removeAllUnRegistered();
            $broadcasts->addMultipleRegistered($broadcastRegister->getLastInsertedItems());
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not insert broadcast: ' . $e->getMessage());
            throw $e;
        }
        //Dispatch events
        if (isset($opInsertBroadcast) || isset($opUpdateBroadcast)) {
            $this->dispatchEvents($broadcasts->getAllRegistered());
        }
        return true;
    }

    /**
     * @param Broadcast[] $broadcasts
     */
    private function dispatchEvents(array $broadcasts): void
    {
        foreach ($broadcasts as $broadcast) {
            $event = new LiveBroadcastEvent();
            $event->setMatch($broadcast->getMatch()->getId());
            $this->dispatcher->dispatch($event, $event->getEventName());
        }
    }

    /**
     * @inheritDoc
     */
    protected function getMappingsFromProviderData(): array
    {
        $mappings = array();
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_MATCH, self::PROVIDER);
        $collection->addId($this->getMatchIdFromXml());
        $mappings[] = $collection;
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_LANGUAGE, self::PROVIDER);
        $collection->addId($this->getLanguageIdFromXml());
        $mappings[] = $collection;
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_BROADCAST_ACTION, self::PROVIDER, true);
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_MATCH_PERIOD, self::PROVIDER, true);
        return $mappings;
    }

    /**
     * @return Collection
     */
    protected function getBroadcastsFromXmlNode(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $collection = new Collection(Broadcast::class);
        $this->getCrawledXmlDocument()->filterXPath('Commentary')
            ->each(function (Crawler $dataXml) use ($mappingCollection, &$collection) {
                $broadcast = $this->createBroadcastFromXml($dataXml);
                if ($mappingCollection->existsOwn(Broadcast::class, $broadcast->getUniqueIdByRelations())) {
                    $broadcast->setId($mappingCollection->getOwn(Broadcast::class, $broadcast->getUniqueIdByRelations()));
                    $collection->addRegistered($broadcast);
                } else {
                    $collection->addUnRegistered($broadcast, $broadcast->getUniqueIdByRelations());
                }
            });
        return $collection;
    }

    /**
     * @param Crawler $node
     * @return Broadcast
     * @throws Exception
     */
    protected function createBroadcastFromXml(Crawler $node): Broadcast
    {
        $broadcast = new Broadcast();
        $broadcast->setMatchById($this->getMatchId());
        $broadcast->setLangById($this->getLanguageId());
        $broadcast->setComments($this->createBroadcastCommentsFromXmlNode($node));
        $broadcast->setManual(true);
        return $broadcast;
    }

    /**
     * @param Crawler $node
     * @return Comment[]
     * @throws Exception
     */
    protected function createBroadcastCommentsFromXmlNode(Crawler $node): array
    {
        $mappingCollection = MappingCollection::getInstance();
        $comments = array();
        $node->filterXPath('Commentary/message')
            ->each(function (Crawler $dataXml) use ($mappingCollection, &$comments) {
                try {
                    $comment = new Comment();
                    //Type
                    if ($mappingCollection->exists(
                        $mappingCollection::ENTITY_BROADCAST_ACTION,
                        self::PROVIDER,
                        $dataXml->attr('type')
                    )) {
                        $comment->setTypeById($mappingCollection->get(
                            $mappingCollection::ENTITY_BROADCAST_ACTION,
                            self::PROVIDER,
                            $dataXml->attr('type'))
                        );
                    }
                    //Period
                    //there are some events which do not have period or have it equal to 0
                    if (!is_null($dataXml->attr('period')) && !empty($dataXml->attr('period'))) {
                        $period = $mappingCollection->get(
                            $mappingCollection::ENTITY_MATCH_PERIOD,
                            self::PROVIDER,
                            $dataXml->attr('period')
                        );
                        //Moment
                        $moment = new Moment();
                        $moment->setPeriodById($period);
                        $categoryPeriod = new Category();
                        $categoryPeriod->setId($period);
                        $moment->setElapsedSeconds($this->getRelativeSecondsForPeriod(
                            $dataXml->attr('minute') * 60 + $dataXml->attr('second'),
                            $categoryPeriod)
                        );
                        $comment->setMoment($moment);
                    }
                    $comment->setText($dataXml->attr('comment'));
                    $comments[] = $comment;
                } catch (MappingException $e) {
                    $this->getLogger()->warning('Mapping error in comments: ' . $e->getMessage());
                    //Keep going, we won't insert that comment
                    return;
                }
            });
        return $comments;
    }

    /**
     * @return string
     * @throws MissingItemException
     */
    protected function getLanguageIdFromXml(): string
    {
        if (!isset($this->langProviderId)) {
            $node = $this->getCrawledXmlDocument()->filterXPath('Commentary');
            if (is_null($node->attr('lang_id'))) {
                throw new MissingItemException('lang_id', 'xml node', 'provider xml file');
            }
            $this->langProviderId = $node->attr('lang_id');
        }
        return $this->langProviderId;
    }

    /**
     * @return string
     * @throws MissingItemException
     */
    protected function getMatchIdFromXml(): string
    {
        if (!isset($this->matchProviderId)) {
            $node = $this->getCrawledXmlDocument()->filterXPath('Commentary');
            if (is_null($node->attr('game_id'))) {
                throw new MissingItemException('game_id', 'xml node', 'provider xml file');
            }
            //For unknown reason opta changes the id in this file and removes de g before the id
            $this->matchProviderId = 'g' . $node->attr('game_id');
        }
        return $this->matchProviderId;
    }
}