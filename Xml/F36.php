<?php

namespace App\Processor\Provider\Opta\Xml;

use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\Resource as MappingResource;
use AsResultados\OAMBundle\Model\Collection\Collection;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Results\Classification\Classification;
use AsResultados\OAMBundle\Model\Results\Classification\Embed\Data;
use Exception;
use Symfony\Component\DomCrawler\Crawler;

class F36 extends F3
{
    /**
     * @return Collection
     * @throws Exception
     */
    protected function getClassificationsFromXml(): Collection
    {
        $classificationArray = array();
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/Competition/TeamStandings[not(Round)]')
            ->each(function (Crawler $nodeClassification) use (&$classificationArray) {
                try {
                    $classificationArray[] = $this->createClassificationFromXmlNode($nodeClassification);
                } catch (Exception $e) {
                    //Do not insert that classification but keep going
                }
            });
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/Competition/TeamStandings/Round')
            ->each(function (Crawler $nodeClassification) use (&$classificationArray) {
                try {
                    $classificationArray[] = $this->createClassificationWithRoundFromXmlNode($nodeClassification);
                } catch (Exception $e) {
                    //Do not insert that classification but keep going
                }

            });
        $classificationArray = $this->getRegisteredItemsCollectionFromArray($classificationArray);
        return $classificationArray;
    }

    /**
     * @param Crawler $node
     * @return Classification
     * @throws Exception
     */
    protected function createClassificationWithRoundFromXmlNode(Crawler $node): Classification
    {
        $mappingCollection = MappingCollection::getInstance();
        $mappingResource = new MappingResource();
        $classification = new Classification();
        $classification->setStageById($this->getStageId());
        $classification->setCompetitionSeasonById($this->getCompetitionSeason()->getId());
        //////////////////////////////////////////////////////
        //MatchDay
        //Searching for the first match day of that stage
        $firstMatchDayStage = null;
        $roundNode = $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument[@Round]');
        if ($roundNode->count() > 0) {
            $round = $roundNode->attr('Round');
        }
        if (isset($round)) {
            $queryTmp = 'MatchInfo[@RoundNumber="' . $round . '"]';
            $query = '//' . $queryTmp . '/@MatchDay[not(. > ../' . $queryTmp . '/@MatchDay)][1]';
            $firstMatchDayStage = $this->getCrawledXmlDocument(
                $this->getFileSystemCalendarPath(
                    $this->getCompetitionFromXml(),
                    $this->getSeasonFromXml()
                )
            )->filterXPath($query);
            if ($firstMatchDayStage->count() > 0) {
                $firstMatchDayStage = $firstMatchDayStage->text();
            }
        }
        $classification->setMatchDay(
            $this->getRealMatchDay($node->parents()->attr('Matchday'), $firstMatchDayStage)
        );
        //MatchDay
        //////////////////////////////////////////////////////

        //////////////////////////////////////////////////////
        /// Group or conference
        $group = $node->attr('Group');
        if (!empty($group)) {
            if ($mappingCollection->exists(
                $mappingResource::ENTITY_CONFERENCE,
                self::PROVIDER,
                $group
            )) {
                $classification->setConferenceById($mappingCollection->get(
                    $mappingResource::ENTITY_CONFERENCE,
                    self::PROVIDER,
                    $group
                ));
            } else {
                $classification->setGroupNumber($this->generateGroupNumber($group));
            }
        }
        /// Group or conference
        //////////////////////////////////////////////////////

        $dataArray = array();
        $node->filterXPath('Round/TeamRecord')
            ->each(function (Crawler $nodeTeam) use (&$dataArray, $mappingCollection) {
                $mappingResource = new MappingResource();
                $data = new Data();
                $data->setTeamById($mappingCollection->get(
                    $mappingResource::ENTITY_TEAM,
                    self::PROVIDER,
                    $nodeTeam->attr('TeamRef')
                ));
                $data->setTotal($this->createClassificationMatchsInfoFromXmlNode($nodeTeam));
                $data->setHome($this->createClassificationMatchsInfoFromXmlNode($nodeTeam, 'home'));
                $data->setAway($this->createClassificationMatchsInfoFromXmlNode($nodeTeam, 'away'));
                $dataArray[] = $data;
            });
        $classification->setData($dataArray);
        return $classification;
    }
}