<?php

namespace App\Processor\Provider\Opta\Xml;

use App\Event\ResourceProcessed\Roster\RosterTeamsEvent;
use App\Event\ResourceProcessed\Team\TeamSquadEvent;
use App\Processor\Provider\Traits\CompetitionSeasonIdTrait;
use App\Processor\Provider\Traits\CompetitionSeasonsTrait;
use App\Utils\StringTrait;
use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\MappingInterface;
use AsResultados\OAMBundle\Api\Internal\Results\Category\CategoryInterface;
use AsResultados\OAMBundle\Api\Internal\Results\RequestsResourceItem;
use AsResultados\OAMBundle\Api\Internal\Results\TeamCompetitionSeason\Request as TeamCompetitionSeasonRequest;
use AsResultados\OAMBundle\Exception\DatabaseInconsistencyException;
use AsResultados\OAMBundle\Exception\MissingItemException;
use AsResultados\OAMBundle\Exception\ValidationItemException;
use AsResultados\OAMBundle\Model\Results\CompetitionSeason\CompetitionSeason;
use AsResultados\OAMBundle\Model\Results\Person\Embed\LastTeam;
use AsResultados\OAMBundle\Model\Results\PersonCompetitionSeason\PersonCompetitionSeason;
use AsResultados\OAMBundle\Api\Internal\Results\PersonCompetitionSeason\Request as PersonCompetitionSeasonRequest;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Results\Person\Embed\Name as PersonName;
use AsResultados\OAMBundle\Model\Results\Team\Embed\Name as TeamName;
use AsResultados\OAMBundle\Model\Results\Person\Person;
use AsResultados\OAMBundle\Model\Results\TeamCompetitionSeason\Embed\Name as TeamCompetitionSeasonName;
use AsResultados\OAMBundle\Model\Results\Team\Team;
use AsResultados\OAMBundle\Model\Collection\Collection;
use AsResultados\OAMBundle\Model\Results\TeamCompetitionSeason\TeamCompetitionSeason;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use AsResultados\OAMBundle\Exception\EmptyItemException;
use AsResultados\OAMBundle\Exception\MappingException;
use App\Utils\Date;
use DateTimeZone;
use Exception;
use Symfony\Component\DomCrawler\Crawler;
use AsResultados\OAMBundle\Api\Internal\Results\Team\Register as TeamRegister;
use AsResultados\OAMBundle\Api\Internal\Results\TeamCompetitionSeason\Register as TeamSeasonRegister;
use AsResultados\OAMBundle\Api\Internal\Results\Person\Register as PersonRegister;
use AsResultados\OAMBundle\Api\Internal\Results\PersonCompetitionSeason\Register as PersonSeasonRegister;

class F40 extends AbstractProcessor
{
    use CompetitionSeasonIdTrait;
    use CompetitionSeasonsTrait;
    use StringTrait;

    /**
     * @var string[]
     */
    protected $providerTeamsProcessed;

    /**
     * Associates teams ids with the competitionSeasons in which they played
     * @var string[]
     */
    protected $teamsInCompetitionSeasons;

    /**
     * @inheritDoc
     */
    protected function setProcessorVariablesFromMapping(): void
    {
        $mappingCollection = MappingCollection::getInstance();
        $this->setCompetitionSeasonId($mappingCollection->get(
            MappingInterface::ENTITY_COMPETITION_SEASON, self::PROVIDER, $this->getCompetitionSeasonFromXml()
        ));
        $this->setCompetitionSeasons([$this->getCompetitionSeasonId()], $this->getClient());
        $this->setMappingsOwn($this->getMappingsOwn());
    }

    /**
     * @inheritDoc
     */
    public function run(): bool
    {
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        /// People who are players and people who are not players are processed independently because
        /// it can be a person who is both a manager and a player in the same season so we have to send it
        /// in two different requests to be able to insert
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        //Get teams
        $teams = $this->getTeamsFromXml();
        $teamRegister = TeamRegister::getInstance($this->getClient());
        //Insert teams
        try {
            $teamRegister->postWithMapping(
                $teams->getAllUnRegistered(), $teams->getUnRegisteredIds(), MappingInterface::ENTITY_TEAM, self::PROVIDER
            );
            $opInsertTeams = true;
            //Add new teams to collection
            $teams->removeAllUnRegistered();
            $teams->addMultipleRegistered($teamRegister->getLastInsertedItems());
        } catch (DatabaseInconsistencyException $e) {
            $this->getLogger()->emergency('Can not insert teams: ' . $e->getMessage());
            throw $e;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not insert teams: ' . $e->getMessage());
        }
        //People
        $personRegister = PersonRegister::getInstance($this->getClient());
        //Get people who are players
        $people = $this->getPeoplePlayerFromXml();
        //Insert people who are players
        try {
            $personRegister->postWithMapping(
                $people->getAllUnRegistered(), $people->getUnRegisteredIds(), MappingInterface::ENTITY_PERSON, self::PROVIDER
            );
            $opInsertPeople = true;
            //Add new people who are players to collection
            $people->removeAllUnRegistered();
            $people->addMultipleRegistered($personRegister->getLastInsertedItems());
        } catch (DatabaseInconsistencyException $e) {
            $this->getLogger()->emergency('Can not insert people who are players: ' . $e->getMessage());
            throw $e;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not insert people who are players: ' . $e->getMessage());
        }
        //Get people who are not players
        $peopleNotPlayers = $this->getPeopleNotPlayerFromXml();
        //Insert people who are not players
        try {
            $personRegister->postWithMapping(
                $peopleNotPlayers->getAllUnRegistered(), $peopleNotPlayers->getUnRegisteredIds(), MappingInterface::ENTITY_PERSON, self::PROVIDER
            );
            $opInsertPeopleNotPlayers = true;
            //Add new people who are not players to collection
            $peopleNotPlayers->removeAllUnRegistered();
            $peopleNotPlayers->addMultipleRegistered($personRegister->getLastInsertedItems());
        } catch (DatabaseInconsistencyException $e) {
            $this->getLogger()->emergency('Can not insert people who are not players: ' . $e->getMessage());
            throw $e;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not insert people who are not players: ' . $e->getMessage());
        }
        //Get teamsCompetitionSeason
        $teamsCompetitionSeason = $this->getTeamsCompetitionsSeasonsFromXml();
        $teamSeasonRegister = TeamSeasonRegister::getInstance($this->getClient());
        //Update teamsCompetitionSeason
        try {
            $teamSeasonRegister->patch($teamsCompetitionSeason->getAllRegistered());
            $opUpdateTeamsCompetitionSeason = true;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update teamsCompetitionSeason: ' . $e->getMessage());
        }
        //Insert teamsCompetitionSeason
        try {
            $teamSeasonRegister->post($teamsCompetitionSeason->getAllUnRegistered());
            $opInsertTeamsCompetitionSeason = true;
            //Add new teamsCompetitionSeason to collection
            $teamsCompetitionSeason->removeAllUnRegistered();
            $teamsCompetitionSeason->addMultipleRegistered($teamSeasonRegister->getLastInsertedItems());
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not insert teamsCompetitionSeason: ' . $e->getMessage());
        }
        //peopleCompetitionSeason
        $personSeasonRegister = PersonSeasonRegister::getInstance($this->getClient());
        //Get peopleCompetitionSeason who are players
        $peopleCompetitionSeason = $this->getPeoplePlayerCompetitionsSeasonsFromXml();
        //Update peopleCompetitionSeason who are players
        try {
            $personSeasonRegister->patch($peopleCompetitionSeason->getAllRegistered());
            $opUpdatePeopleCompetitionSeason = true;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update peopleCompetitionSeason who are players: ' . $e->getMessage());
        }
        //Insert peopleCompetitionSeason who are players
        try {
            $personSeasonRegister->post($peopleCompetitionSeason->getAllUnRegistered());
            $opInsertPeopleCompetitionSeason = true;
            //Add new peopleCompetitionSeason who are players to collection
            $peopleCompetitionSeason->removeAllUnRegistered();
            $peopleCompetitionSeason->addMultipleRegistered($personSeasonRegister->getLastInsertedItems());
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not insert peopleCompetitionSeason who are players: ' . $e->getMessage());
        }
        //Get peopleCompetitionSeason who are not players
        $peopleCompetitionSeasonNotPlayers = $this->getPeopleNotPlayerCompetitionsSeasonsFromXml();
        //Update peopleCompetitionSeason who are not players
        try {
            $personSeasonRegister->patch($peopleCompetitionSeasonNotPlayers->getAllRegistered());
            $opUpdatePeopleCompetitionSeasonNotPlayers = true;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update peopleCompetitionSeason who are not players: ' . $e->getMessage());
        }
        //Insert peopleCompetitionSeason who are not players
        try {
            $personSeasonRegister->post($peopleCompetitionSeasonNotPlayers->getAllUnRegistered());
            $opInsertPeopleCompetitionSeasonNotPlayers = true;
            //Add new peopleCompetitionSeason who are not players to collection
            $peopleCompetitionSeasonNotPlayers->removeAllUnRegistered();
            $peopleCompetitionSeasonNotPlayers->addMultipleRegistered($personSeasonRegister->getLastInsertedItems());
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not insert peopleCompetitionSeason who are not players: ' . $e->getMessage());
        }
        //Update teamsCompetitionSeason inside teams
        $teamsCompetitionSeason->eachRegistered(function (TeamCompetitionSeason $teamCompetitionSeason) use (&$teams) {
            if ($teams->existsRegistered($teamCompetitionSeason->getTeam()->getId())) {
                $tmp = $teams->getRegistered($teamCompetitionSeason->getTeam()->getId());
                if ($tmp instanceof Team) {
                    $competitionsSeasons = $tmp->getCompetitionsSeasons();
                    if (!is_array($competitionsSeasons)) {
                        $competitionsSeasons = array();
                    }
                    $competitionsSeasons[] = $teamCompetitionSeason->getId();
                    $tmp->setCompetitionsSeasons($competitionsSeasons);
                }
            }
        });
        //Update teams
        try {
            $teamRegister->patch($teams->getAllRegistered());
            $opUpdateTeams = true;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update teams: ' . $e->getMessage());
        }
        //Update peopleCompetitionSeason who are players inside people
        $peopleCompetitionSeason->eachRegistered(function (PersonCompetitionSeason $personCompetitionSeason) use (&$people) {
            if ($people->existsRegistered($personCompetitionSeason->getPerson()->getId())) {
                $tmp = $people->getRegistered($personCompetitionSeason->getPerson()->getId());
                if ($tmp instanceof Person) {
                    $competitionsSeasons = $tmp->getCompetitionsSeasons();
                    if (!is_array($competitionsSeasons)) {
                        $competitionsSeasons = array();
                    }
                    $competitionsSeasons[] = $personCompetitionSeason;
                    $tmp->setCompetitionsSeasons($competitionsSeasons);
                }
            }
        });
        //Update peopleCompetitionSeason who are not players inside people
        $peopleCompetitionSeasonNotPlayers->eachRegistered(function (PersonCompetitionSeason $personCompetitionSeason) use (&$peopleNotPlayers) {
            if ($peopleNotPlayers->existsRegistered($personCompetitionSeason->getPerson()->getId())) {
                $tmp = $peopleNotPlayers->getRegistered($personCompetitionSeason->getPerson()->getId());
                if ($tmp instanceof Person) {
                    $competitionsSeasons = $tmp->getCompetitionsSeasons();
                    if (!is_array($competitionsSeasons)) {
                        $competitionsSeasons = array();
                    }
                    $competitionsSeasons[] = $personCompetitionSeason;
                    $tmp->setCompetitionsSeasons($competitionsSeasons);
                }
            }
        });
        //Calculate last team only if we have currently playing competitions
        if (!empty($this->getCompetitionSeasonsIdsStatusPlaying())) {
            foreach ($people->getAllRegistered() as &$person) {
                try {
                    $this->calculateLastTeamForPerson($person);
                } catch (Exception $e) {
                    //Keep going
                    continue;
                }
            }
            foreach ($peopleNotPlayers->getAllRegistered() as &$person) {
                try {
                    $this->calculateLastTeamForPerson($person);
                } catch (Exception $e) {
                    //Keep going
                    continue;
                }
            }
        }
        //Update people who are players
        try {
            $personRegister->patch($people->getAllRegistered());
            $opUpdatePeople = true;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update people who are players: ' . $e->getMessage());
        }
        //Update people who are not players
        try {
            $personRegister->patch($peopleNotPlayers->getAllRegistered());
            $opUpdatePeopleNotPlayers = true;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update people who are not players: ' . $e->getMessage());
        }
        //Dispatch events
        if (isset($opInsertTeams) || isset($opInsertTeamsCompetitionSeason) ||
            isset($opUpdateTeams) || isset($opUpdateTeamsCompetitionSeason)) {
            $this->dispatchEventsTeams();
        }
        if (isset($opInsertPeople) || isset($opInsertPeopleNotPlayers) ||
            isset($opInsertPeopleCompetitionSeason) || isset($opInsertPeopleCompetitionSeasonNotPlayers) ||
            isset($opUpdatePeople) || isset($opUpdatePeopleNotPlayers) ||
            isset($opUpdatePeopleCompetitionSeason) || isset($opUpdatePeopleCompetitionSeasonNotPlayers)) {
            $this->dispatchEventsPeople();
        }
        if (isset($opInsertTeams) || isset($opInsertTeamsCompetitionSeason)) {
            $this->dispatchEventsInsertTeams();
        }
        if (isset($opInsertPeople) || isset($opInsertPeopleCompetitionSeason) ||
            isset($opInsertPeopleNotPlayers) || isset($opInsertPeopleCompetitionSeasonNotPlayers)) {
            $this->dispatchEventsInsertPeople();
        }
        return true;
    }

    private function dispatchEventsTeams(): void
    {
        $mappingCollection = MappingCollection::getInstance();
        foreach ($this->getCompetitionSeasons() as $competitionSeason) {
            foreach ($mappingCollection->getMultiple($mappingCollection::ENTITY_TEAM, self::PROVIDER) as $item) {
                //Events team calendar
                $event = new TeamSquadEvent();
                $event->setTeam($item);
                $event->setSeason($competitionSeason->getSeason()->getId());
                $this->dispatcher->dispatch($event, $event->getEventName());
            }
            //Only one competitionSeason
            break;
        }
    }

    private function dispatchEventsPeople(): void
    {
    }

    private function dispatchEventsInsertTeams(): void
    {
        foreach ($this->getCompetitionSeasons() as $competitionSeason) {
            $event = new RosterTeamsEvent();
            $event->setCompetitionSeason($competitionSeason->getId());
            $this->dispatcher->dispatch($event, $event->getEventName());
        }
    }

    private function dispatchEventsInsertPeople(): void
    {
    }

    /**
     * @inheritDoc
     */
    protected function getMappingsFromProviderData(): array
    {
        $mappings = array();
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_COMPETITION_SEASON, self::PROVIDER);
        $collection->addId($this->getCompetitionSeasonFromXml());
        $mappings[] = $collection;
        $mappings[] = $this->getMappingTeamsFromXml();
        $mappings[] = $this->getMappingPersonsFromXml();
        $mappings[] = $this->getMappingCountriesFromXml();
        $mappings[] = $this->getMappingVenuesFromXml();
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_PREFERRED_FOOT, self::PROVIDER, true);
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_ROL, self::PROVIDER, true);
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_PERSON_TYPE, self::PROVIDER, true);
        return $mappings;
    }

    /**
     * @throws Exception
     */
    protected function getMappingsOwn(): array
    {
        $requests = array();
        $mappingCollection = MappingCollection::getInstance();
        $requestTeam = new TeamCompetitionSeasonRequest();
        $requestPerson = new PersonCompetitionSeasonRequest();
        foreach ($this->getCompetitionSeasons() as $competitionSeason) {
            $requests[] = new RequestsResourceItem(
                $requestTeam->getByTeamsAndCompetitionSeason(
                    $mappingCollection->getMultiple(MappingInterface::ENTITY_TEAM, self::PROVIDER),
                    $competitionSeason->getId()
                ),
                $requestTeam->getResource()
            );
            $requests[] = new RequestsResourceItem(
                $requestPerson->getByPeopleAndCompetitionSeason(
                    $mappingCollection->getMultiple(MappingInterface::ENTITY_PERSON, self::PROVIDER),
                    $competitionSeason->getId()
                ),
                $requestPerson->getResource()
            );
        }
        return $requests;
    }

    /**
     * @return Collection
     */
    protected function getTeamsFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(Team::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/Team')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                try {
                    $id = $node->attr('uID');
                    if (empty($id)) {
                        //No id, skip it
                        return;
                    }
                    $item = $this->createTeamFromXmlNode($node);
                    if ($mappingCollection->exists($mappingCollection::ENTITY_TEAM, self::PROVIDER, $id)) {
                        $item->setId($mappingCollection->get($mappingCollection::ENTITY_TEAM, self::PROVIDER, $id));
                        //Set slug to null because it should be already inserted
                        //$item->setSlug(null);
                    }
                    if ($mappingCollection->exists($mappingCollection::ENTITY_TEAM, self::PROVIDER, $id)) {
                        $result->addRegistered($item);
                    } else {
                        $result->addUnRegistered($item, $id);
                    }
                    //Set competition data
                    if ($this->getCompetitionSeason($this->getCompetitionSeasonId())->getCompetition()->getType()->getId() === CategoryInterface::COMPETITION_TYPE_NATIONAL_TEAM) {
                        $item->setNationalTeam(true);
                    } else {
                        $item->setNationalTeam(false);
                    }
                    $item->setGender($this->getCompetitionSeason($this->getCompetitionSeasonId())->getCompetition()->getGender());
                    $this->providerTeamsProcessed[] = $id;
                } catch (ValidationItemException $e) {
                    //Do nothing, it should be an opta ghost team, skip item and keep going
                } catch (MappingException $e) {
                    //Mapping error, skip item and keep going
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return Team
     * @throws MappingException
     * @throws ValidationItemException
     */
    protected function createTeamFromXmlNode(Crawler $node): Team
    {
        $mappingCollection = MappingCollection::getInstance();
        $team = new Team();
        //If there is no country we skip it (it should be a pre_known_team)
        if (empty($node->attr('country'))) {
            throw new ValidationItemException('missing country', 'country should be defined', 'f40 in team: ' . $node->attr('uID'));
        }
        if ($mappingCollection->exists($mappingCollection::ENTITY_COUNTRY, self::PROVIDER, $node->attr('country'))) {
            $team->setCountryById($mappingCollection->get($mappingCollection::ENTITY_COUNTRY, self::PROVIDER, $node->attr('country')));
        } else {
            $this->getLogger()->warning(
                'Mapping mismatch for country: ' . $node->attr('country'),
                [$this->getLogger()::CHANNEL_EDITORIAL]);
        }
        $data = $node->filterXPath('Team/Founded');
        if ($data->count() > 0) {
            $team->setYearFoundation($data->text());
        }
        $name = new TeamName();
        $data = $node->attr('official_club_name');
        if (!empty($data)) {
            $name->setOfficial($data);
        }
        $data = $node->filterXPath('Team/Name');
        if ($data->count() > 0) {
            $name->setKnown($data->text());
        } else {
            $data = $node->attr('short_club_name');
            if (!empty($data)) {
                $name->setKnown($data);
            }
        }
        $data = $node->filterXPath('Team/SYMID');
        if ($data->count() > 0) {
            $name->setAbbreviation($data->text());
        }
        $team->setName($name);
        if (!is_null($name->getOfficial())) {
            $team->setSlug($this->normalizeString($name->getOfficial() . '_' . $name->getAbbreviation()));
        } else {
            $team->setSlug($this->normalizeString($name->getKnown() . '_' . $name->getAbbreviation()));
        }
        return $team;
    }

    /**
     * @return Collection
     */
    protected function getTeamsCompetitionsSeasonsFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(TeamCompetitionSeason::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/Team')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                try {
                    $itemTmp = $this->createTeamsCompetitionsSeasonsFromXmlNode($node);
                    $competitionSeasons = $this->getTeamCompetitionSeasons($itemTmp->getTeam()->getId());
                    foreach ($competitionSeasons as $competitionSeason) {
                        $item = clone($itemTmp);
                        $competitionSeasonObject = new CompetitionSeason();
                        $competitionSeasonObject->setId($competitionSeason);
                        $item->setCompetitionSeason($competitionSeasonObject);
                        if ($mappingCollection->existsOwn(TeamCompetitionSeason::class, $item->getUniqueIdByRelations())) {
                            $item->setId($mappingCollection->getOwn(TeamCompetitionSeason::class, $item->getUniqueIdByRelations()));
                            $result->addRegistered($item);
                        } else {
                            $result->addUnRegistered($item, $item->getUniqueIdByRelations());
                        }
                    }
                } catch (MissingItemException $e) {
                    //Skip item and keep going
                } catch (MappingException $e) {
                    //Mapping error, skip item and keep going
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return TeamCompetitionSeason
     * @throws MappingException
     */
    protected function createTeamsCompetitionsSeasonsFromXmlNode(Crawler $node): TeamCompetitionSeason
    {
        $mappingCollection = MappingCollection::getInstance();
        $teamCompetitionSeason = new TeamCompetitionSeason();
        $teamCompetitionSeason->setTeamById($mappingCollection->get(
            $mappingCollection::ENTITY_TEAM,
            self::PROVIDER,
            $node->attr('uID')
        ));
        $name = new TeamCompetitionSeasonName();
        $data = $node->attr('short_club_name');
        if (!empty($data)) {
            $name->setKnown($data);
        } else {
            $data = $node->filterXPath('Team/Name');
            if ($data->count() > 0) {
                $name->setKnown($data->text());
            }
        }
        $teamCompetitionSeason->setName($name);
        $data = $node->filterXPath('Team/Stadium');
        if ($data->count() > 0) {
            if ($mappingCollection->exists($mappingCollection::ENTITY_VENUE, self::PROVIDER, $data->attr('uID'))) {
                $teamCompetitionSeason->setVenueById($mappingCollection->get(
                    $mappingCollection::ENTITY_VENUE,
                    self::PROVIDER,
                    $data->attr('uID')));
            }
        }
        return $teamCompetitionSeason;
    }

    /**
     * @return Collection
     */
    protected function getPeoplePlayerFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(Person::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/Team/Player | SoccerFeed/SoccerDocument/PlayerChanges/Team/Player')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                try {
                    $id = $node->attr('uID');
                    if (empty($id)) {
                        //No id, skip it
                        return;
                    }
                    $item = $this->createPersonPlayerFromXmlNode($node);
                    if ($mappingCollection->exists($mappingCollection::ENTITY_PERSON, self::PROVIDER, $id)) {
                        $item->setId($mappingCollection->get($mappingCollection::ENTITY_PERSON, self::PROVIDER, $id));
                        //Set slug to null because it should be already inserted
                        //$item->setSlug(null);
                    }
                    if ($mappingCollection->exists($mappingCollection::ENTITY_PERSON, self::PROVIDER, $id)) {
                        $result->addRegistered($item);
                    } else {
                        $result->addUnRegistered($item, $id);
                    }
                } catch (ValidationItemException $e) {
                    //Do nothing, skip item and keep going
                } catch (MappingException $e) {
                    //Mapping error, skip item and keep going
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return Person
     * @throws MappingException
     * @throws ValidationItemException
     */
    protected function createPersonPlayerFromXmlNode(Crawler $node): Person
    {
        //Check if we have processed its team, if not, skip it
        if (!in_array($node->parents()->attr('uID'), $this->providerTeamsProcessed)) {
            throw new ValidationItemException('team not processed', 'player should have a processed team', 'f40 id: ' . $node->attr('uID'));
        }
        $mappingCollection = MappingCollection::getInstance();
        $person = new Person();
        $name = new PersonName();
        $data = $node->filterXPath('Player/Stat[@Type="first_name"]');
        if ($data->count() > 0 && !is_null($this->checkValue($data->text()))) {
            $name->setFirst($data->text());
        }
        $data = $node->filterXPath('Player/Stat[@Type="last_name"]');
        if ($data->count() > 0 && !is_null($this->checkValue($data->text()))) {
            $name->setLast($data->text());
        }
        $data = $node->filterXPath('Player/Stat[@Type="known_name"]');
        if ($data->count() > 0 && !is_null($this->checkValue($data->text()))) {
            $name->setKnown($data->text());
        } else {
            $data = $node->filterXPath('Player/Name');
            if ($data->count() > 0 && !is_null($this->checkValue($data->text()))) {
                $name->setKnown($data->text());
            }
        }
        $person->setName($name);
        if (!is_null($name->getFirst()) && !is_null($name->getLast())) {
            $person->setSlug($this->normalizeString($name->getFirst() . ' ' . $name->getLast()));
        } else {
            $person->setSlug($this->normalizeString($name->getKnown()));
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Country & first nationalization
        /// First nationality is the birth country of the player if it exists and country the nationality.
        /// Otherwise, if first nationality does not exists, country represents both values
        $data = $node->filterXPath('Player/Stat[@Type="country"]');
        if ($data->count() > 0 && !is_null($this->checkValue($data->text()))) {
            if ($mappingCollection->exists($mappingCollection::ENTITY_COUNTRY, self::PROVIDER, $data->text())) {
                $country = $mappingCollection->get(
                    $mappingCollection::ENTITY_COUNTRY,
                    self::PROVIDER,
                    $data->text()
                );
                $person->setNationalizationById($country);
                $person->setCountryById($country);
            }
        }
        $data = $node->filterXPath('Player/Stat[@Type="first_nationality"]');
        if ($data->count() > 0 && !is_null($this->checkValue($data->text()))) {
            if ($mappingCollection->exists($mappingCollection::ENTITY_COUNTRY, self::PROVIDER, $data->text())) {
                $person->setCountryById($mappingCollection->get(
                    $mappingCollection::ENTITY_COUNTRY,
                    self::PROVIDER,
                    $data->text()
                ));
            }
        }
        /// Country & first nationalization
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        $data = $node->filterXPath('Player/Stat[@Type="birth_date"]');
        if ($data->count() > 0 && !is_null($this->checkValue($data->text()))) {
            try {
                $date = new Date($data->text(), new DateTimeZone(self::DATETIME_ZONE));
                $person->setBirthDate($date->format($date::FORMAT_Ymd));
            } catch (Exception $e) {
                //Do nothing, date will not be set
            }
        }
        $data = $node->filterXPath('Player/Stat[@Type="preferred_foot"]');
        if ($data->count() > 0 && !is_null($this->checkValue($data->text()))) {
            if ($mappingCollection->exists($mappingCollection::ENTITY_PREFERRED_FOOT, self::PROVIDER, $data->text())) {
                $person->setPreferredFoot($mappingCollection->get(
                    $mappingCollection::ENTITY_PREFERRED_FOOT,
                    self::PROVIDER,
                    $data->text()));
            }
        }
        return $person;
    }

    /**
     * @return Collection
     */
    protected function getPeoplePlayerCompetitionsSeasonsFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(PersonCompetitionSeason::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/Team/Player | SoccerFeed/SoccerDocument/PlayerChanges/Team/Player')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                try {
                    $itemTmp = $this->createPeopleCompetitionsSeasonsFromXmlNode($node);
                    $competitionSeasons = $this->getTeamCompetitionSeasons($itemTmp->getTeam()->getId());
                    foreach ($competitionSeasons as $competitionSeason) {
                        $item = clone($itemTmp);
                        $competitionSeasonObject = new CompetitionSeason();
                        $competitionSeasonObject->setId($competitionSeason);
                        $item->setCompetitionSeason($competitionSeasonObject);
                        if ($mappingCollection->existsOwn(PersonCompetitionSeason::class, $item->getUniqueIdByRelations())) {
                            $item->setId($mappingCollection->getOwn(PersonCompetitionSeason::class, $item->getUniqueIdByRelations()));
                            $result->addRegistered($item);
                        } else {
                            $result->addUnRegistered($item, $item->getUniqueIdByRelations());
                        }
                    }
                } catch (MissingItemException $e) {
                    //Skip item and keep going
                } catch (MappingException $e) {
                    //Mapping error, skip item and keep going
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return PersonCompetitionSeason
     * @throws MappingException
     */
    protected function createPeopleCompetitionsSeasonsFromXmlNode(Crawler $node): PersonCompetitionSeason
    {
        $mappingCollection = MappingCollection::getInstance();
        $personCompetitionSeason = new PersonCompetitionSeason();
        $personCompetitionSeason->setPersonById($mappingCollection->get(
            $mappingCollection::ENTITY_PERSON,
            self::PROVIDER,
            $node->attr('uID')
        ));
        $personCompetitionSeason->setTeamById($mappingCollection->get(
            $mappingCollection::ENTITY_TEAM,
            self::PROVIDER,
            $node->parents()->attr('uID')
        ));
        $personCompetitionSeason->setTypeById(CategoryInterface::PERSON_TYPE_PLAYER);
        if ($node->attr('loan')) {
            $personCompetitionSeason->setLoan(true);
        } else {
            $personCompetitionSeason->setLoan(false);
        }
        $data = $node->filterXPath('Player/Stat[@Type="known_name"]');
        if ($data->count() > 0 && !is_null($this->checkValue($data->text()))) {
            $personCompetitionSeason->setName($this->checkValue($data->text()));
        } else {
            $data = $node->filterXPath('Player/Name');
            if ($data->count() > 0 && !is_null($this->checkValue($data->text()))) {
                $personCompetitionSeason->setName($this->checkValue($data->text()));
            }
        }
        $data = $node->filterXPath('Player/Stat[@Type="leave_date"]');
        if ($data->count() > 0) {
            $personCompetitionSeason->setActive(false);
        } else {
            $personCompetitionSeason->setActive(true);
        }
        $data = $node->filterXPath('Player/Position');
        if ($data->count() > 0 && !is_null($this->checkValue($data->text()))) {
            if ($mappingCollection->exists($mappingCollection::ENTITY_ROL, self::PROVIDER, $data->text())) {
                $personCompetitionSeason->setRolById($mappingCollection->get(
                    $mappingCollection::ENTITY_ROL,
                    self::PROVIDER,
                    $data->text()
                ));
            }
        }
        $data = $node->filterXPath('Player/Stat[@Type="jersey_num"]');
        if ($data->count() > 0 && !is_null($this->checkValue($data->text()))) {
            $personCompetitionSeason->setJerseyNum((int)$data->text());
        }
        $data = $node->filterXPath('Player/Stat[@Type="weight"]');
        if ($data->count() > 0 && !is_null($this->checkValue($data->text()))) {
            $personCompetitionSeason->setWeight((int)$data->text());
        }
        $data = $node->filterXPath('Player/Stat[@Type="height"]');
        if ($data->count() > 0 && !is_null($this->checkValue($data->text()))) {
            $personCompetitionSeason->setHeight((int)$data->text());
        }
        return $personCompetitionSeason;
    }

    /**
     * @return Collection
     */
    protected function getPeopleNotPlayerFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(Person::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/Team/TeamOfficial | SoccerFeed/SoccerDocument/PlayerChanges/Team/TeamOfficial')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                try {
                    $id = $node->attr('uID');
                    if (empty($id)) {
                        //No id, skip it
                        return;
                    }
                    $item = $this->createPersonNotPlayerFromXmlNode($node);
                    if ($mappingCollection->exists($mappingCollection::ENTITY_PERSON, self::PROVIDER, $id)) {
                        $item->setId($mappingCollection->get($mappingCollection::ENTITY_PERSON, self::PROVIDER, $id));
                        //Set slug to null because it should be already inserted
                        //$item->setSlug(null);
                    }
                    if ($mappingCollection->exists($mappingCollection::ENTITY_PERSON, self::PROVIDER, $id)) {
                        $result->addRegistered($item);
                    } else {
                        $result->addUnRegistered($item, $id);
                    }
                } catch (ValidationItemException $e) {
                    //Do nothing, skip item and keep going
                } catch (MappingException $e) {
                    //Mapping error, skip item and keep going
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return Person
     * @throws MappingException
     * @throws ValidationItemException
     */
    protected function createPersonNotPlayerFromXmlNode(Crawler $node): Person
    {
        //Check if we have processed its team, if not, skip it
        if (!in_array($node->parents()->attr('uID'), $this->providerTeamsProcessed)) {
            throw new ValidationItemException('team not processed', 'player should have a processed team', 'f40 id: ' . $node->attr('uID'));
        }
        $mappingCollection = MappingCollection::getInstance();
        $person = new Person();
        $name = new PersonName();
        $data = $node->filterXPath('TeamOfficial/PersonName/First');
        if ($data->count() > 0 && !is_null($this->checkValue($data->text()))) {
            $name->setFirst($data->text());
        }
        $data = $node->filterXPath('TeamOfficial/PersonName/Last');
        if ($data->count() > 0 && !is_null($this->checkValue($data->text()))) {
            $name->setLast($data->text());
            $name->setKnown($data->text());
        }
        $person->setName($name);
        $person->setSlug($this->normalizeString($name->getFirst() . ' ' . $name->getLast()));
        if ($mappingCollection->exists($mappingCollection::ENTITY_COUNTRY, self::PROVIDER, $node->attr('country'))) {
            $person->setCountryById($mappingCollection->get(
                $mappingCollection::ENTITY_COUNTRY,
                self::PROVIDER,
                $node->attr('country')
            ));
        }
        $data = $node->filterXPath('TeamOfficial/PersonName/BirthDate');
        if ($data->count() > 0 && !is_null($this->checkValue($data->text()))) {
            try {
                $date = new Date($data->text(), new DateTimeZone(self::DATETIME_ZONE));
                $person->setBirthDate($date->format($date::FORMAT_Ymd));
            } catch (Exception $e) {
                //Do nothing, date will not be set
            }
        }
        return $person;
    }

    /**
     * @return Collection
     */
    protected function getPeopleNotPlayerCompetitionsSeasonsFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(PersonCompetitionSeason::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/Team/TeamOfficial | SoccerFeed/SoccerDocument/PlayerChanges/Team/TeamOfficial')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                try {
                    $itemTmp = $this->createPeopleNotPlayerCompetitionsSeasonsFromXmlNode($node);
                    $competitionSeasons = $this->getTeamCompetitionSeasons($itemTmp->getTeam()->getId());
                    foreach ($competitionSeasons as $competitionSeason) {
                        $item = clone($itemTmp);
                        $competitionSeasonObject = new CompetitionSeason();
                        $competitionSeasonObject->setId($competitionSeason);
                        $item->setCompetitionSeason($competitionSeasonObject);
                        if ($mappingCollection->existsOwn(PersonCompetitionSeason::class, $item->getUniqueIdByRelations())) {
                            $item->setId($mappingCollection->getOwn(PersonCompetitionSeason::class, $item->getUniqueIdByRelations()));
                            $result->addRegistered($item);
                        } else {
                            $result->addUnRegistered($item, $item->getUniqueIdByRelations());
                        }
                    }
                } catch (MissingItemException $e) {
                    //Skip item and keep going
                } catch (MappingException $e) {
                    //Mapping error, skip item and keep going
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return PersonCompetitionSeason
     * @throws MappingException
     */
    protected function createPeopleNotPlayerCompetitionsSeasonsFromXmlNode(Crawler $node): PersonCompetitionSeason
    {
        $mappingCollection = MappingCollection::getInstance();
        $personCompetitionSeason = new PersonCompetitionSeason();
        $personCompetitionSeason->setPersonById($mappingCollection->get(
            $mappingCollection::ENTITY_PERSON,
            self::PROVIDER,
            $node->attr('uID')
        ));
        $personCompetitionSeason->setTeamById($mappingCollection->get(
            $mappingCollection::ENTITY_TEAM,
            self::PROVIDER,
            $node->parents()->attr('uID')
        ));
        $name = '';
        $data = $node->filterXPath('TeamOfficial/PersonName/First');
        if ($data->count() > 0 && !is_null($this->checkValue($data->text()))) {
            $name = $data->text();
        }
        $data = $node->filterXPath('TeamOfficial/PersonName/Last');
        if ($data->count() > 0 && !is_null($this->checkValue($data->text()))) {
            $name .= empty($name) ? $data->text() : ' ' . $data->text();
        }
        $personCompetitionSeason->setName($name);
        $personCompetitionSeason->setTypeById($mappingCollection->get(
            $mappingCollection::ENTITY_PERSON_TYPE,
            self::PROVIDER,
            $node->attr('Type')
        ));
        $personCompetitionSeason->setLoan(false);
        $data = $node->filterXPath('TeamOfficial/PersonName/leave_date');
        if ($data->count() > 0) {
            $personCompetitionSeason->setActive(false);
        } else {
            $personCompetitionSeason->setActive(true);
        }
        return $personCompetitionSeason;
    }

    /**
     * @param Person $person
     * @throws Exception
     */
    protected function calculateLastTeamForPerson(Person $person): void
    {
        try {
            $personCompetitionSeasons = $person->getCompetitionsSeasons();
            if (is_array($personCompetitionSeasons)) {
                foreach ($personCompetitionSeasons as $personCompetitionSeason) {
                    if (in_array($personCompetitionSeason->getCompetitionSeason()->getId(), $this->getCompetitionSeasonsIdsStatusPlaying())) {
                        $competitionSeason = $this->getCompetitionSeason($personCompetitionSeason->getCompetitionSeason()->getId());
                        $lastTeam = new LastTeam();
                        $lastTeam->setSeason($competitionSeason->getSeason()->getId());
                        $lastTeam->setTeam($personCompetitionSeason->getTeam());
                        $person->setLastTeam($lastTeam);
                        break;
                    }
                }
                throw new Exception('Any personCompetitionSeasons match with current playing competitionSeaons');
            } else {
                throw new Exception('No personCompetitionSeasons inside people');
            }
        } catch (Exception $e) {
            throw new Exception('Unable to calculate last team for person: ' . $person->getId() . ' - Error: ' . $e->getMessage());
        }
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingTeamsFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_TEAM, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/Team[@uID]')
            ->each(function (Crawler $node) use ($collection) {
                $collection->addId($node->attr('uID'));
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingPersonsFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_PERSON, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/Team/Player[@uID] |
                                SoccerFeed/SoccerDocument/Team/TeamOfficial[@uID] |
                                SoccerFeed/SoccerDocument/PlayerChanges/Team/Player[@uID] |
                                SoccerFeed/SoccerDocument/PlayerChanges/Team/TeamOfficial[@uID]')
            ->each(function (Crawler $node) use ($collection) {
                $collection->addId($node->attr('uID'));
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingCountriesFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_COUNTRY, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/Team/@country |
                                SoccerFeed/SoccerDocument/Team/Player/Stat[@Type="country"]')
            ->each(function (Crawler $node) use ($collection) {
                try {
                    $country = $node->text();
                    $collection->addId($country);
                } catch (Exception $e) {
                    return;
                }
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingVenuesFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_VENUE, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/Team/Stadium[@uID]')
            ->each(function (Crawler $node) use ($collection) {
                $collection->addId($node->attr('uID'));
            });
        return $collection;
    }

    /**
     * @param string $id
     * @return string[]
     * @throws MissingItemException
     * @throws Exception
     */
    protected function getTeamCompetitionSeasons(string $id): array
    {
        if (!isset($this->teamsInCompetitionSeasons)) {
            $mappingCollection = MappingCollection::getInstance();
            $result = array();
            $this->getCrawledXmlDocument()
                ->filterXPath('SoccerFeed/SoccerDocument/Team[@uID]')
                ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                    if (!$mappingCollection->exists($mappingCollection::ENTITY_TEAM, self::PROVIDER, $node->attr('uID'))) {
                        //We do not have that mapping, very strange but keep going
                        return;
                    }
                    $id = $mappingCollection->get($mappingCollection::ENTITY_TEAM, self::PROVIDER, $node->attr('uID'));
                    if (!isset($result[$id][$this->getCompetitionSeasonId()])) {
                        $result[$id][$this->getCompetitionSeasonId()] = $this->getCompetitionSeasonId();
                    }
                });
            $this->teamsInCompetitionSeasons = $result;
        }
        if (!isset($this->teamsInCompetitionSeasons[$id])) {
            throw new MissingItemException($id, 'teamsInCompetitionSeason', 'can not associate team id to any competitionSeason');
        }
        return $this->teamsInCompetitionSeasons[$id];
    }
}