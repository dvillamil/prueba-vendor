<?php

namespace App\Processor\Provider\Opta\Xml;

use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\MappingInterface;
use AsResultados\OAMBundle\Api\Internal\Results\Category\CategoryInterface;
use AsResultados\OAMBundle\Api\Internal\Results\Match\Register as MatchRegister;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Results\Category\Category;
use AsResultados\OAMBundle\Model\Results\Match\Embed\Card;
use AsResultados\OAMBundle\Model\Results\Match\Embed\Goal;
use AsResultados\OAMBundle\Model\Results\Match\Embed\Goals;
use AsResultados\OAMBundle\Model\Results\Match\Embed\PenaltiesShootOut;
use AsResultados\OAMBundle\Model\Results\Match\Embed\PenaltyShootOut;
use AsResultados\OAMBundle\Model\Results\Match\Embed\Substitution;
use AsResultados\OAMBundle\Model\Results\Match\Embed\SubstitutionPlayer;
use AsResultados\OAMBundle\Model\Results\Match\Embed\Team;
use AsResultados\OAMBundle\Model\Results\Match\Match;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use App\Event\ResourceProcessed\Calendar\CalendarEvent;
use App\Event\ResourceProcessed\Carousel\CarouselEvent;
use AsResultados\OAMBundle\Exception\MissingItemException;
use App\Processor\Provider\Traits\MatchTrait;
use AsResultados\OAMBundle\Exception\EmptyItemException;
use AsResultados\OAMBundle\Exception\MappingException;
use App\Utils\FootballTrait;
use AsResultados\OAMBundle\Model\Results\Moment;
use Exception;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class F26 extends AbstractProcessor
{
    use FootballTrait;
    use MatchTrait;

    private const DETAILS_ACCEPTED = array(
        CategoryInterface::MATCH_DETAIL_BASIC,
        CategoryInterface::MATCH_DETAIL_BASIC_GOALS,
        CategoryInterface::MATCH_DETAIL_BASIC_GOALS_CARDS_SUBS
    );

    /**
     * @inheritDoc
     * @throws ExceptionInterface
     */
    protected function setProcessorVariablesFromMapping(): void
    {
        $mappingCollection = MappingCollection::getInstance();
        $this->setMatchesDetails(
            $mappingCollection->getMultiple(
                MappingInterface::ENTITY_MATCH, self::PROVIDER
            ),
            $this->getClient()
        );
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function run(): bool
    {
        $matches = $this->getMatchesFromXml();
        $matchRegister = MatchRegister::getInstance($this->getClient());
        //Update match
        try {
            //Patch instead of put because we do not change structure data (matchday, groupnumber, stage ... etc) and other things
            $matchRegister->patch($matches);
            $opUpdateMatch = true;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update matches: ' . $e->getMessage());
        }
        //Dispatch events
        if (isset($opUpdateMatch)) {
            $this->dispatchEvents($matches);
        }
        return true;
    }

    /**
     * @param Match[] $matches
     */
    private function dispatchEvents(array $matches): void
    {
        /** @var Match[] $matchesDifferentCalendar */
        $matchesDifferentCalendar = array();
        /** @var Match[] $matchesDifferentCarousel */
        $matchesDifferentCarousel = array();
        foreach ($matches as $match) {
            $matchesDifferentCalendar[$match->getCompetitionSeason()->getId()] = $match;
            $matchesDifferentCarousel[$match->getStage()->getId() . '_' . $match->getMatchDay()] = $match;
        }
        //Event calendar
        foreach ($matchesDifferentCalendar as $matchDifferent) {
            $event = new CalendarEvent();
            $event->setCompetitionSeason($matchDifferent->getCompetitionSeason()->getId());
            $this->dispatcher->dispatch($event, $event->getEventName());
        }
        //Event carousel
        foreach ($matchesDifferentCarousel as $matchDifferent) {
            $event = new CarouselEvent();
            $event->setCompetitionSeason($matchDifferent->getCompetitionSeason()->getId());
            $event->setStage($matchDifferent->getStage()->getId());
            $event->setMatchDay($matchDifferent->getMatchDay());
            $this->dispatcher->dispatch($event, $event->getEventName());
        }
    }

    /**
     * @inheritDoc
     */
    protected function getMappingsFromProviderData(): array
    {
        $mappings = array();
        $mappings[] = $this->getMappingTeamsFromXml();
        $mappings[] = $this->getMappingPeopleFromXml();
        $mappings[] = $this->getMappingMatchesFromXml();
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_MATCH_STATE, self::PROVIDER, true);
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_MATCH_PERIOD, self::PROVIDER, true);
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_SUBSTITUTION_REASON, self::PROVIDER, true);
        return $mappings;
    }

    /**
     * @return Match[]
     * @throws Exception
     */
    protected function getMatchesFromXml(): array
    {
        $result = array();
        $this->getCrawledXmlDocument()
            ->filterXPath('feed/content.item/content.body/results/result')
            ->each(function (Crawler $node) use (&$result) {
                $id = $node->attr('game-id');
                if (empty($id)) {
                    //No id, skip it
                    return;
                }
                try {
                    $item = $this->createMatchFromXmlNode($node);
                    $result[] = $item;
                } catch (MappingException $e) {
                    $this->getLogger()->alert('We can not process match: ' . $id . ' - Error: ' . $e->getMessage());
                    $this->getLogger()->alert(
                        'We can not process match due to mapping: ' . $id . ' - Error: ' . $e->getMessage(),
                        [$this->getLogger()::CHANNEL_EDITORIAL]
                    );
                    //Keep going, we will try to process other matches
                    return;
                } catch (MissingItemException $e) {
                    $this->getLogger()->alert('We can not process match: ' . $id . ' - Error: ' . $e->getMessage());
                    //Keep going, we will try to process other matches
                    return;
                }

            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return Match
     * @throws MappingException
     * @throws MissingItemException
     */
    protected function createMatchFromXmlNode(Crawler $node): Match
    {
        $mappingCollection = MappingCollection::getInstance();
        $match = new Match();
        //If no mapping for match, we can not process the match
        //For unknown reason opta does not add the g letter in the id of the match, so we add it manually
        $idProvider = 'g' . $node->attr('game-id');
        if (!$mappingCollection->exists($mappingCollection::ENTITY_MATCH, self::PROVIDER, $idProvider)) {
            throw new MappingException(self::PROVIDER, $idProvider, $mappingCollection::ENTITY_MATCH);
        }
        $id = $mappingCollection->get($mappingCollection::ENTITY_MATCH, self::PROVIDER, $idProvider);
        $match->setId($id);
        //State
        $match->setStateById($mappingCollection->get($mappingCollection::ENTITY_MATCH_STATE, self::PROVIDER, $node->attr('match-status')));
        //Teams
        $teamXml = $node->filterXPath('result/home-team');
        if ($teamXml->count() > 0) {
            $match->setLocal($this->createMatchTeamFromXmlNode($teamXml));
        }
        $teamXml = $node->filterXPath('result/away-team');
        if ($teamXml->count() > 0) {
            $match->setVisitor($this->createMatchTeamFromXmlNode($teamXml));
        }
        //Set things retrieved from the bd and necessary later on
        if (!isset($this->getMatches()[$match->getId()])) {
            throw new MissingItemException($match->getId(), Match::class, 'matches retrieved from bd');
        }
        $match->setCompetitionSeason($this->getMatches()[$match->getId()]->getCompetitionSeason());
        $match->setStage($this->getMatches()[$match->getId()]->getStage());
        $match->setMatchDay($this->getMatches()[$match->getId()]->getMatchDay());
        //Incomplete data, we only set it if detail level is appropriate
        if (($this->getMatches()[$id]) && in_array($this->getMatches()[$id]->getDetail()->getId(), self::DETAILS_ACCEPTED)) {
            $match = $this->addIncompleteDataMatchFromXmlNode($node, $match);
        }
        return $match;
    }

    /**
     * @param Crawler $node
     * @param Match $match
     * @return Match
     * @throws MappingException
     */
    protected function addIncompleteDataMatchFromXmlNode(Crawler $node, Match $match): Match
    {
        $mappingCollection = MappingCollection::getInstance();
        //Detail
        $match->setDetailById(CategoryInterface::MATCH_DETAIL_BASIC_GOALS_CARDS_SUBS);
        //Moment
        $moment = new Moment();
        $moment->setPeriodById($mappingCollection->get($mappingCollection::ENTITY_MATCH_PERIOD, self::PROVIDER, $node->attr('period')));
        $match->setMoment($moment);
        return $match;
    }

    /**
     * @param Crawler $node
     * @return Team
     * @throws MappingException
     * @throws Exception
     */
    protected function createMatchTeamFromXmlNode(Crawler $node): Team
    {
        $mappingCollection = MappingCollection::getInstance();
        $team = new Team();
        //Team id
        $id = $node->filterXPath('//team-id');
        if ($id->count() !== 1) {
            throw new Exception('No id for team');
        }
        //We have to add the first t manually as opta do not write it here but it is in its uid
        $id = 't' . $id->text();
        $team->setTeamById($mappingCollection->get(
            $mappingCollection::ENTITY_TEAM,
            self::PROVIDER,
            $id
        ));
        //Goals
        try {
            $data = $this->createMatchTeamGoalsFromXmlNode($node);
            $team->setGoals($data);
        } catch (Exception $e) {
            $this->getLogger()->warning('Can not insert goals: ' . $e->getMessage());
        }
        //Cards
        try {
            $data = $this->createMatchTeamCardsFromXmlNode($node);
            //We add this always although it may be empty because maybe there has been an error and we have to eliminate
            $team->setCards($data);
        } catch (Exception $e) {
            $this->getLogger()->warning('Can not insert cards: ' . $e->getMessage());
        }
        //Penalties
        try {
            $data = $this->createMatchTeamPenaltiesFromXmlNode($node);
            $team->setPenaltiesShoutOut($data);
        } catch (Exception $e) {
            $this->getLogger()->warning('Can not insert penalties: ' . $e->getMessage());
        }
        //Substitutions
        try {
            $data = $this->createMatchTeamSubstitutionsFromXmlNode($node);
            //We add this always although it may be empty because maybe there has been an error and we have to eliminate
            $team->setSubstitutions($data);
        } catch (Exception $e) {
            $this->getLogger()->warning('Can not insert substitutions: ' . $e->getMessage());
        }
        return $team;
    }

    /**
     * @param Crawler $node
     * @return Goals
     */
    protected function createMatchTeamGoalsFromXmlNode(Crawler $node): Goals
    {
        $mappingCollection = MappingCollection::getInstance();
        $goals = new Goals();
        $goalsDetails = array();
        //If period is penalty shootout we skip it (we have to insert them in penalties and not in goals)
        $node->filterXPath('//scorers/scorer[not(@period="ShootOut")]')
            ->each(function (Crawler $dataXml) use ($mappingCollection, &$goalsDetails) {
                $goal = new Goal();
                //Person
                $id = $dataXml->filterXPath('//player-code');
                if ($id->count() !== 1) {
                    throw new Exception('No id for player');
                }
                //We have to add the first p manually as opta do not write it but it is in its uid
                $id = 'p' . $id->text();
                $goal->setPlayerById($mappingCollection->get(
                    $mappingCollection::ENTITY_PERSON,
                    self::PROVIDER,
                    $id)
                );
                $this->setGoalType($goal, $dataXml->attr('goal-type'));
                //Period
                if ($mappingCollection->exists(
                    $mappingCollection::ENTITY_MATCH_PERIOD,
                    self::PROVIDER,
                    $dataXml->attr('period')
                )) {
                    $period = $mappingCollection->get(
                        $mappingCollection::ENTITY_MATCH_PERIOD,
                        self::PROVIDER,
                        $dataXml->attr('period')
                    );
                    $moment = new Moment();
                    $moment->setPeriodById($period);
                    $categoryPeriod = new Category();
                    $categoryPeriod->setId($period);
                    $moment->setElapsedSeconds($this->getRelativeSecondsForPeriod(
                        $dataXml->attr('min') * 60 + $dataXml->attr('sec'),
                        $categoryPeriod)
                    );
                    $goal->setMoment($moment);
                }
                $goalsDetails[] = $goal;
            });
        //We add this always although it may be empty because maybe there has been an error and we have to eliminate
        $goals->setDetails($goalsDetails);
        $goals->setScore(count($goalsDetails));
        return $goals;
    }

    /**
     * @param Crawler $node
     * @return Card[]
     */
    protected function createMatchTeamCardsFromXmlNode(Crawler $node): array
    {
        $mappingCollection = MappingCollection::getInstance();
        $cards = array();
        //Opta uses two different structures for yellow and red cards so we have to search for both independently
        //Red cards
        $node->filterXPath('//bookings/red-card')
            ->each(function (Crawler $dataXml) use ($mappingCollection, &$cards) {
                $card = new Card();
                //Person
                $id = $dataXml->filterXPath('//player-code');
                if ($id->count() !== 1) {
                    throw new Exception('No id for player');
                }
                //We have to add the first p manually as opta do not write it but it is in its uid
                $id = 'p' . $id->text();
                $card->setPlayerById($mappingCollection->get(
                    $mappingCollection::ENTITY_PERSON,
                    self::PROVIDER,
                    $id
                ));
                //Card Type
                //Take a look searching if it is a straight red or the second yellow
                if (strtolower($dataXml->attr('type')) === 'second yellow') {
                    $card->setTypeById(CategoryInterface::BOOKING_CARD_YELLOW_SECOND);
                } else {
                    $card->setTypeById(CategoryInterface::BOOKING_CARD_RED);
                }
                //Period
                if ($mappingCollection->exists(
                    $mappingCollection::ENTITY_MATCH_PERIOD,
                    self::PROVIDER,
                    $dataXml->attr('period')
                )) {
                    $period = $mappingCollection->get(
                        $mappingCollection::ENTITY_MATCH_PERIOD,
                        self::PROVIDER,
                        $dataXml->attr('period')
                    );
                    $moment = new Moment();
                    $moment->setPeriodById($period);
                    $categoryPeriod = new Category();
                    $categoryPeriod->setId($period);
                    $moment->setElapsedSeconds($this->getRelativeSecondsForPeriod(
                        $dataXml->attr('min') * 60 + $dataXml->attr('sec'),
                        $categoryPeriod)
                    );
                    $card->setMoment($moment);
                }
                $cards[] = $card;
            });
        //Yellow cards
        $node->filterXPath('//bookings/yellow-card')
            ->each(function (Crawler $dataXml) use ($mappingCollection, &$cards) {
                $card = new Card();
                //Person
                //We have to add the first p manually as opta do not write it but it is in its uid
                $id = 'p' . $dataXml->attr('id');
                $card->setPlayerById($mappingCollection->get(
                    $mappingCollection::ENTITY_PERSON,
                    self::PROVIDER,
                    $id
                ));
                //Card Type
                $card->setTypeById(CategoryInterface::BOOKING_CARD_YELLOW);
                //Period
                if ($mappingCollection->exists(
                    $mappingCollection::ENTITY_MATCH_PERIOD,
                    self::PROVIDER,
                    $dataXml->attr('period')
                )) {
                    $period = $mappingCollection->get(
                        $mappingCollection::ENTITY_MATCH_PERIOD,
                        self::PROVIDER,
                        $dataXml->attr('period')
                    );
                    $moment = new Moment();
                    $moment->setPeriodById($period);
                    $categoryPeriod = new Category();
                    $categoryPeriod->setId($period);
                    $moment->setElapsedSeconds($this->getRelativeSecondsForPeriod(
                        $dataXml->attr('min') * 60 + $dataXml->attr('sec'),
                        $categoryPeriod)
                    );
                    $card->setMoment($moment);
                }
                $cards[] = $card;
            });
        return $cards;
    }

    /**
     * @param Crawler $node
     * @return Substitution[]
     */
    protected function createMatchTeamSubstitutionsFromXmlNode(Crawler $node): array
    {
        $mappingCollection = MappingCollection::getInstance();
        $substitutions = array();
        $node->filterXPath('//substitutions/substitution')
            ->each(function (Crawler $dataXml) use ($mappingCollection, &$substitutions) {
                $substitution = new Substitution();
                $substitutionPlayers = new SubstitutionPlayer();
                //Person in
                $id = $dataXml->filterXPath('//sub-on/player-code');
                if ($id->count() !== 1) {
                    throw new Exception('No id for player');
                }
                //We have to add the first p manually as opta do not write it but it is in its uid
                $id = 'p' . $id->text();
                $substitutionPlayers->setInById($mappingCollection->get(
                    $mappingCollection::ENTITY_PERSON,
                    self::PROVIDER,
                    $id
                ));
                //Person out
                $id = $dataXml->filterXPath('//sub-off/player-code');
                if ($id->count() !== 1) {
                    throw new Exception('No id for player');
                }
                //We have to add the first p manually as opta do not write it but it is in its uid
                $id = 'p' . $id->text();
                $substitutionPlayers->setOutById($mappingCollection->get(
                    $mappingCollection::ENTITY_PERSON,
                    self::PROVIDER,
                    $id
                ));
                $substitution->setPlayers($substitutionPlayers);
                //Reason
                $substitution->setReasonById($mappingCollection->get(
                    $mappingCollection::ENTITY_SUBSTITUTION_REASON,
                    self::PROVIDER,
                    $dataXml->attr('reason'))
                );
                //Period
                if ($mappingCollection->exists(
                    $mappingCollection::ENTITY_MATCH_PERIOD,
                    self::PROVIDER,
                    $dataXml->attr('period')
                )) {
                    $period = $mappingCollection->get(
                        $mappingCollection::ENTITY_MATCH_PERIOD,
                        self::PROVIDER,
                        $dataXml->attr('period')
                    );
                    $moment = new Moment();
                    $moment->setPeriodById($period);
                    $categoryPeriod = new Category();
                    $categoryPeriod->setId($period);
                    $moment->setElapsedSeconds($this->getRelativeSecondsForPeriod(
                        $dataXml->attr('min') * 60 + $dataXml->attr('sec'),
                        $categoryPeriod)
                    );
                    $substitution->setMoment($moment);
                }
                $substitutions[] = $substitution;
            });
        return $substitutions;
    }

    /**
     * @param Crawler $node
     * @return PenaltiesShootOut
     */
    protected function createMatchTeamPenaltiesFromXmlNode(Crawler $node): PenaltiesShootOut
    {
        $mappingCollection = MappingCollection::getInstance();
        $penalties = new PenaltiesShootOut();
        $penaltiesDetails = array();
        $node->filterXPath('//scorers/scorer[@period="ShootOut"]')
            ->each(function (Crawler $dataXml) use ($mappingCollection, &$penalties) {
                $penalty = new PenaltyShootOut();
                //Person
                $id = $dataXml->filterXPath('//player-code');
                if ($id->count() !== 1) {
                    throw new Exception('No id for player');
                }
                //We have to add the first p manually as opta do not write it but it is in its uid
                $id = 'p' . $id->text();
                $penalty->setPlayerById($mappingCollection->get(
                    $mappingCollection::ENTITY_PERSON,
                    self::PROVIDER,
                    $id
                ));
                //Shoot
                //Always scored because they do not send the others
                $penalty->setShootById(CategoryInterface::PENALTY_SHOOT_SCORED);
                //Period
                $categoryPeriod = new Category();
                $moment = new Moment();
                $moment->setPeriodById($categoryPeriod::MATCH_PERIOD_SHOOT_OUT);
                $categoryPeriod->setId($categoryPeriod::MATCH_PERIOD_SHOOT_OUT);
                $moment->setElapsedSeconds($this->getRelativeSecondsForPeriod(
                    $dataXml->attr('min') * 60 + $dataXml->attr('sec'),
                    $categoryPeriod)
                );
                $penalty->setMoment($moment);
                $penaltiesDetails[] = $penalty;
            });
        //We add this always although it may be empty because maybe there has been an error and we have to eliminate
        $penalties->setDetails($penaltiesDetails);
        $penalties->setScore(count($penaltiesDetails));
        return $penalties;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingMatchesFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_MATCH, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('feed/content.item/content.body/results/result[@game-id]')
            ->each(function (Crawler $node) use ($collection) {
                //For unknown reason opta does not add the g letter in the id of the match, so we add it manually
                $id = 'g' . $node->attr('game-id');
                $collection->addId($id);
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingTeamsFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_TEAM, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('//team-id')
            ->each(function (Crawler $node) use ($collection) {
                //For unknown reason opta does not add the t letter in the id of the team, so we add it manually
                $id = 't' . $node->text();
                $collection->addId($id);
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingPeopleFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_PERSON, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('//player-code')
            ->each(function (Crawler $node) use ($collection) {
                //For unknown reason opta does not add the p letter in the id of the player, so we add it manually
                $id = 'p' . $node->text();
                $collection->addId($id);
            });
        $this->getCrawledXmlDocument()
            ->filterXPath('//yellow-card[@id]')
            ->each(function (Crawler $node) use ($collection) {
                //For unknown reason opta does not add the p letter in the id of the player, so we add it manually
                $id = 'p' . $node->attr('id');
                $collection->addId($id);
            });
        return $collection;
    }
}