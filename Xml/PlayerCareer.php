<?php

namespace App\Processor\Provider\Opta\Xml;

use AsResultados\OAMBundle\Api\Internal\Results\Category\CategoryInterface;
use AsResultados\OAMBundle\Api\Internal\Results\RequestsResourceItem;
use AsResultados\OAMBundle\Api\Internal\Results\TeamCompetitionSeason\Request as TeamCompetitionSeasonRequest;
use AsResultados\OAMBundle\Api\Internal\Results\PersonCompetitionSeason\Request as PersonCompetitionSeasonRequest;
use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\MappingInterface;
use AsResultados\OAMBundle\Api\Internal\Results\Competition\Register as CompetitionRegister;
use AsResultados\OAMBundle\Api\Internal\Results\CompetitionSeason\Register as CompetitionSeasonRegister;
use AsResultados\OAMBundle\Api\Internal\Results\TeamCompetitionSeason\Register as TeamCompetitionSeasonRegister;
use AsResultados\OAMBundle\Api\Internal\Results\PersonCompetitionSeason\Register as PersonCompetitionSeasonRegister;
use AsResultados\OAMBundle\Api\Internal\Results\Team\Register as TeamRegister;
use AsResultados\OAMBundle\Api\Internal\Results\Person\Register as PersonRegister;
use AsResultados\OAMBundle\Model\Results\Competition\Competition;
use AsResultados\OAMBundle\Model\Results\CompetitionSeason\CompetitionSeason;
use AsResultados\OAMBundle\Model\Results\PersonCompetitionSeason\Embed\Stat as PersonStat;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Collection\Collection;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use AsResultados\OAMBundle\Model\Results\PersonCompetitionSeason\PersonCompetitionSeason;
use AsResultados\OAMBundle\Model\Results\Team\Embed\Name as TeamName;
use AsResultados\OAMBundle\Model\Results\Team\Team;
use AsResultados\OAMBundle\Model\Results\TeamCompetitionSeason\TeamCompetitionSeason;
use AsResultados\OAMBundle\Model\Results\TeamCompetitionSeason\Embed\Name as TeamSeasonName;
use AsResultados\OAMBundle\Exception\MappingException;
use AsResultados\OAMBundle\Exception\EmptyItemException;
use App\Processor\Provider\Traits\PersonTrait;
use Exception;
use Symfony\Component\DomCrawler\Crawler;

class PlayerCareer extends AbstractProcessor
{
    use PersonTrait;

    /**
     * @var string
     */
    protected $personProviderId;

    /**
     * @var string
     */
    protected $personProviderKnown;

    /**
     * @var array
     */
    protected $stats = array(
        'goals',
        'assists',
        'penaltyGoals',
        'appearances',
        'yellowCards',
        'secondYellowCards',
        'redCards',
        'substituteIn',
        'substituteOut',
        'minutesPlayed'
    );

    protected function setProcessorVariablesFromMapping(): void
    {
        $mappingCollection = MappingCollection::getInstance();
        $this->setPerson(
            $mappingCollection->get(MappingInterface::ENTITY_PERSON, self::PROVIDER, $this->getPersonIdFromXml()),
            $this->getClient()
        );
        $this->setMappingsOwn($this->getMappingsOwn());
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function run(): bool
    {
        //Add non existent competitions
        $data = $this->getCompetitionsFromXml();
        if (count($data->getAllUnRegistered()) > 0) {
            try {
                $register = CompetitionRegister::getInstance($this->getClient());
                $register->postWithMapping(
                    $data->getAllUnRegistered(), $data->getUnRegisteredIds(), MappingInterface::ENTITY_COMPETITION, self::PROVIDER
                );
            } catch (EmptyItemException $e) {
            } catch (Exception $e) {
                $this->getLogger()->error('Can not insert competitions: ' . $e->getMessage());
                throw $e;
            }
        }
        //Add non existent competitionsSeasons
        $data = $this->getCompetitionsSeasonsFromXml();
        if (count($data->getAllUnRegistered()) > 0) {
            try {
                $register = CompetitionSeasonRegister::getInstance($this->getClient());
                $register->postWithMapping(
                    $data->getAllUnRegistered(), $data->getUnRegisteredIds(), MappingInterface::ENTITY_COMPETITION_SEASON, self::PROVIDER
                );
            } catch (EmptyItemException $e) {
            } catch (Exception $e) {
                $this->getLogger()->error('Can not insert competitionsSeasons: ' . $e->getMessage());
                throw $e;
            }
        }
        //Add non existent teams
        $teams = $this->getTeamsFromXml();
        if (count($teams->getAllUnRegistered()) > 0) {
            try {
                $register = TeamRegister::getInstance($this->getClient());
                $register->postWithMapping(
                    $teams->getAllUnRegistered(), $teams->getUnRegisteredIds(), MappingInterface::ENTITY_TEAM, self::PROVIDER
                );
                //Add new teams to collection
                $teams->removeAllUnRegistered();
                $teams->addMultipleRegistered($register->getLastInsertedItems());
            } catch (EmptyItemException $e) {
            } catch (Exception $e) {
                $this->getLogger()->error('Can not insert teams: ' . $e->getMessage());
                throw $e;
            }
        }
        //Add non existent teamsCompetitionSeason
        $data = $this->getTeamsCompetitionsSeasonsFromXml();
        if (count($data->getAllUnRegistered()) > 0) {
            try {
                $register = TeamCompetitionSeasonRegister::getInstance($this->getClient());
                $register->postWithMapping(
                    $data->getAllUnRegistered(), $data->getUnRegisteredIds(), MappingInterface::ENTITY_TEAM, self::PROVIDER
                );
            } catch (EmptyItemException $e) {
            } catch (Exception $e) {
                $this->getLogger()->error('Can not insert teamsCompetitionsSeasons: ' . $e->getMessage());
                throw $e;
            }
            try {
                //Update teamsCompetitionSeason inside teams
                $data->eachRegistered(function (TeamCompetitionSeason $teamCompetitionSeason) use (&$teams) {
                    if ($teams->existsRegistered($teamCompetitionSeason->getTeam()->getId())) {
                        $tmp = $teams->getRegistered($teamCompetitionSeason->getTeam()->getId());
                        if ($tmp instanceof Team) {
                            $tmp->setCompetitionsSeasons([$teamCompetitionSeason->getId()]);
                        }
                    }
                });
                //Update teams
                $register = TeamRegister::getInstance($this->getClient());
                $register->patch($teams->getAllRegistered());
            } catch (EmptyItemException $e) {
            } catch (Exception $e) {
                $this->getLogger()->error('Can not update teams to insert their competitionSeasons: ' . $e->getMessage());
                throw $e;
            }
        }
        //Add non existent personCompetitionSeason
        $data = $this->getPersonCompetitionsSeasonsFromXml();
        $register = PersonCompetitionSeasonRegister::getInstance($this->getClient());
        if (count($data->getAllUnRegistered()) > 0) {
            try {
                $register->postWithMapping(
                    $data->getAllUnRegistered(), $data->getUnRegisteredIds(), MappingInterface::ENTITY_PERSON, self::PROVIDER
                );
            } catch (EmptyItemException $e) {
            } catch (Exception $e) {
                $this->getLogger()->error('Can not insert personCompetitionsSeasons: ' . $e->getMessage());
                throw $e;
            }
            try {
                //Update personCompetitionSeason inside person
                $person = $this->getPerson();
                $person->setCompetitionsSeasons($register->getLastInsertedIds());
                //Update person
                $register = PersonRegister::getInstance($this->getClient());
                $register->patch([$person]);
            } catch (EmptyItemException $e) {
            } catch (Exception $e) {
                $this->getLogger()->error('Can not update person to insert its competitionSeasons: ' . $e->getMessage());
                throw $e;
            }
        }
        return true;
    }

    /**
     * @return ProviderIdsCollection[]
     * @throws Exception
     */
    protected function getMappingsFromProviderData(): array
    {
        $mappings = array();
        $mappings[] = $this->getMappingCompetitionsFromXml();
        $mappings[] = $this->getMappingCompetitionsSeasonsFromXml();
        $mappings[] = $this->getMappingTeamsFromXml();
        $mappings[] = $this->getMappingPeopleFromXml();
        $stats = new ProviderIdsCollection(MappingInterface::ENTITY_STAT_PLAYER, self::PROVIDER, true);
        $stats->setIds($this->getStats());
        $mappings[] = $stats;
        return $mappings;
    }

    /**
     * @throws Exception
     */
    protected function getMappingsOwn(): array
    {
        $requests = array();
        $mappingCollection = MappingCollection::getInstance();
        $request = new TeamCompetitionSeasonRequest();
        $requests[] = new RequestsResourceItem(
            $request->getByTeams($mappingCollection->getMultiple(MappingInterface::ENTITY_TEAM, self::PROVIDER)),
            $request->getResource()
        );
        $request = new PersonCompetitionSeasonRequest();
        $requests[] = new RequestsResourceItem(
            $request->getByPeople($mappingCollection->getMultiple(MappingInterface::ENTITY_PERSON, self::PROVIDER)),
            $request->getResource()
        );
        return $requests;
    }

    /**
     * @return Collection
     * @throws Exception
     */
    protected function getCompetitionsFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(Competition::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('playerCareer/person/membership/stat')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                try {
                    $id = $node->attr('opCompetitionId');
                    if (empty($id)) {
                        $this->getLogger()->warning('Skipping competition: ' . $node->attr('competitionName') . ' - Do not have id');
                        return;
                    }
                    $item = $this->createCompetitionFromXmlNode($node);
                    if ($mappingCollection->exists($mappingCollection::ENTITY_COMPETITION, self::PROVIDER, $id)) {
                        $item->setId($mappingCollection->get($mappingCollection::ENTITY_COMPETITION, self::PROVIDER, $id));
                        $result->addRegistered($item);
                    } else {
                        $result->addUnRegistered($item, $id);
                    }
                } catch (Exception $e) {
                    //Notify but keep going
                    $this->getLogger()->warning('Skipping item competition: ' . $e->getMessage());
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return Competition
     * @throws Exception
     */
    protected function createCompetitionFromXmlNode(Crawler $node): Competition
    {
        $competition = new Competition();
        $competition->setName($node->attr('competitionName'));
        $competition->setFriendly($this->getIsFriendly($node->attr('isFriendly')));
        $competition->setTypeById($this->getCompetitionType($node->parents()->attr('contestantType')));
        $competition->setGender($this->getGender($node->parents()->attr('type')));
        return $competition;
    }

    /**
     * @return Collection
     * @throws Exception
     */
    protected function getCompetitionsSeasonsFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(CompetitionSeason::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('playerCareer/person/membership/stat')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                try {
                    $competitionId = $node->attr('opCompetitionId');
                    if (empty($competitionId)) {
                        throw new EmptyItemException('competition id', 'Competition node with name: ' . $node->attr('competitionName'));
                    }
                    $item = $this->createCompetitionSeasonFromXmlNode($node);
                    $id = $this->generateProviderCompetitionSeasonId($competitionId, $item->getSeason());
                    if (!$mappingCollection->exists($mappingCollection::ENTITY_COMPETITION_SEASON, self::PROVIDER, $id)) {
                        $result->addUnRegistered($item, $id);
                    }
                } catch (Exception $e) {
                    //Notify but keep going
                    $this->getLogger()->warning('Skipping item competition season: ' . $e->getMessage());
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return CompetitionSeason
     * @throws EmptyItemException
     * @throws MappingException
     */
    protected function createCompetitionSeasonFromXmlNode(Crawler $node): CompetitionSeason
    {
        $mappingCollection = MappingCollection::getInstance();
        $competitionSeason = new CompetitionSeason();
        $competitionId = $node->attr('opCompetitionId');
        if (empty($competitionId)) {
            throw new EmptyItemException('competition id', 'Competition node with name: ' . $node->attr('competitionName'));
        }
        $competitionSeason->setCompetitionById($mappingCollection->get($mappingCollection::ENTITY_COMPETITION, self::PROVIDER, $competitionId));
        $seasonId = $node->attr('tournamentCalendarName');
        //Season id can be written in two ways:
        //1) a year (2015)
        //2) a mix of years (2015/2016)
        $seasonId = preg_match('/(\d)+/', $seasonId, $seasonIdYears);
        //Error or lack of matching
        if ($seasonId !== 1) {
            throw new EmptyItemException('season id', 'Trophy node with name: ' . $node->attr('tournamentCalendarName'));
        }
        $competitionSeason->setSeason($seasonIdYears[0]);
        $competitionSeason->setName($node->attr('competitionName') . ' ' . $node->attr('tournamentCalendarName'));
        return $competitionSeason;
    }

    /**
     * @return Collection
     * @throws Exception
     */
    protected function getTeamsFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(Team::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('playerCareer/person/membership')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                try {
                    $id = $node->attr('opContestantId');
                    if (empty($id)) {
                        throw new EmptyItemException('team id', 'Team node with name: ' . $node->attr('contestantName'));
                    }
                    $item = $this->createTeamFromXmlNode($node);
                    if (!$mappingCollection->exists($mappingCollection::ENTITY_TEAM, self::PROVIDER, $id)) {
                        $result->addUnRegistered($item, $id);
                    } else {
                        $result->addRegistered($item);
                    }
                } catch (MappingException | EmptyItemException $e) {
                    $this->getLogger()->warning('Skipping team: ' . $e->getMessage());
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return Team
     */
    protected function createTeamFromXmlNode(Crawler $node): Team
    {
        $team = new Team();
        $name = new TeamName();
        $name->setOfficial($node->attr('contestantName'));
        $name->setKnown($node->attr('contestantName'));
        $team->setName($name);
        return $team;
    }

    /**
     * @return Collection
     * @throws Exception
     */
    protected function getTeamsCompetitionsSeasonsFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(TeamCompetitionSeason::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('playerCareer/person/membership/stat')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                try {
                    //We only need teamsCompetitionSeason which are not in the system
                    $item = $this->createTeamCompetitionSeasonFromXmlNode($node);
                    if (!$mappingCollection->existsOwn(TeamCompetitionSeason::class, $item->getUniqueIdByRelations())) {
                        $result->addUnRegistered($item, $item->getUniqueIdByRelations());
                    }
                } catch (MappingException | EmptyItemException $e) {
                    $this->getLogger()->warning('Skipping teamCompetitionSeason: ' . $e->getMessage());
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return TeamCompetitionSeason
     * @throws EmptyItemException
     * @throws MappingException
     * @throws Exception
     */
    protected function createTeamCompetitionSeasonFromXmlNode(Crawler $node): TeamCompetitionSeason
    {
        $mappingCollection = MappingCollection::getInstance();
        $teamCompetitionSeason = new TeamCompetitionSeason();
        $competitionId = $node->attr('opCompetitionId');
        if (empty($competitionId)) {
            throw new EmptyItemException('competition id', 'Competition node with name: ' . $node->attr('competitionName'));
        }
        $seasonId = $node->attr('tournamentCalendarName');
        //Season id can be written in two ways:
        //1) a year (2015)
        //2) a mix of years (2015/2016)
        $seasonId = preg_match('/(\d)+/', $seasonId, $seasonIdYears);
        //Error or lack of matching
        if ($seasonId !== 1) {
            throw new EmptyItemException('season id', 'Stat node with name: ' . $node->attr('tournamentCalendarName'));
        }
        $teamCompetitionSeason->setCompetitionSeasonById(
            $mappingCollection->get(
                MappingInterface::ENTITY_COMPETITION_SEASON,
                self::PROVIDER,
                $this->generateProviderCompetitionSeasonId($competitionId, $seasonIdYears[0])
            )
        );
        $teamId = $node->parents()->attr('opContestantId');
        if (empty($teamId)) {
            throw new EmptyItemException('team id', 'Competition node with name: ' . $node->attr('competitionName'));
        }
        $teamCompetitionSeason->setTeamById(
            $mappingCollection->get(
                MappingInterface::ENTITY_TEAM,
                self::PROVIDER,
                $node->parents()->attr('opContestantId')
            )
        );
        $name = new TeamSeasonName();
        $name->setKnown($node->parents()->attr('contestantName'));
        $teamCompetitionSeason->setName($name);
        return $teamCompetitionSeason;
    }

    /**
     * @return Collection
     * @throws Exception
     */
    protected function getPersonCompetitionsSeasonsFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(PersonCompetitionSeason::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('playerCareer/person/membership/stat')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                try {
                    //We only need personCompetitionsSeasons which are not in the system
                    $item = $this->createPersonCompetitionSeasonFromXmlNode($node);
                    if (!$mappingCollection->existsOwn(PersonCompetitionSeason::class, $item->getUniqueIdByRelations())) {
                        $result->addUnRegistered($item, $item->getUniqueIdByRelations());
                    }
                } catch (MappingException | EmptyItemException $e) {
                    $this->getLogger()->warning('Skipping personCompetitionSeason: ' . $e->getMessage());
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return PersonCompetitionSeason
     * @throws EmptyItemException
     * @throws MappingException
     * @throws Exception
     */
    protected function createPersonCompetitionSeasonFromXmlNode(Crawler $node): PersonCompetitionSeason
    {
        $mappingCollection = MappingCollection::getInstance();
        $personCompetitionSeason = new PersonCompetitionSeason();
        $competitionId = $node->attr('opCompetitionId');
        if (empty($competitionId)) {
            throw new EmptyItemException('competition id', 'Competition node with name: ' . $node->attr('competitionName'));
        }
        $seasonId = $node->attr('tournamentCalendarName');
        //Season id can be written in two ways:
        //1) a year (2015)
        //2) a mix of years (2015/2016)
        $seasonId = preg_match('/(\d)+/', $seasonId, $seasonIdYears);
        //Error or lack of matching
        if ($seasonId !== 1) {
            throw new EmptyItemException('season id', 'Stat node with name: ' . $node->attr('tournamentCalendarName'));
        }
        $personCompetitionSeason->setCompetitionSeasonById(
            $mappingCollection->get(
                MappingInterface::ENTITY_COMPETITION_SEASON,
                self::PROVIDER,
                $this->generateProviderCompetitionSeasonId($competitionId, $seasonIdYears[0])
            )
        );
        $teamId = $node->parents()->attr('opContestantId');
        if (empty($teamId)) {
            throw new EmptyItemException('team id', 'Stat node with name: ' . $node->attr('tournamentCalendarName'));
        }
        $personCompetitionSeason->setTeamById(
            $mappingCollection->get(
                MappingInterface::ENTITY_TEAM,
                self::PROVIDER,
                $teamId
            )
        );
        $personCompetitionSeason->setTypeById(CategoryInterface::PERSON_TYPE_PLAYER);
        $personCompetitionSeason->setPersonById($this->getPerson()->getId());
        $data = $node->attr('shirtNumber');
        if (!empty($data)) {
            $personCompetitionSeason->setJerseyNum($data);
        }
        $personCompetitionSeason->setName($this->getPersonKnownFromXml());
        $stats = array();
        foreach ($this->getStats() as $statName) {
            $data = $node->attr($statName);
            if (empty($data)) {
                //Empty, keep going
                continue;
            }
            if (!$mappingCollection->exists(
                    $mappingCollection::ENTITY_STAT_PLAYER,
                    self::PROVIDER,
                    $statName
                ) || empty($data)) {
                //We do not have mapping for that stat, keep going
                $this->getLogger()->warning('There is no mapping for stat: ' . $statName);
                continue;
            }
            $stat = new PersonStat();
            $stat->setStatById($mappingCollection->get(
                $mappingCollection::ENTITY_STAT_PLAYER,
                self::PROVIDER,
                $statName
            ));
            $stat->setValue($data);
            $stats[] = $stat;
        }
        if (!empty($stats)) {
            $personCompetitionSeason->setStats($stats);
        }
        return $personCompetitionSeason;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingCompetitionsFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_COMPETITION, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('playerCareer/person/membership/stat')
            ->each(function (Crawler $node) use ($collection) {
                $id = $node->attr('opCompetitionId');
                //No id, skip it
                if (empty($id)) {
                    return;
                }
                $collection->addId($id);
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingCompetitionsSeasonsFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_COMPETITION_SEASON, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('playerCareer/person/membership/stat')
            ->each(function (Crawler $node) use ($collection) {
                $competitionId = $node->attr('opCompetitionId');
                //No id, skip it
                if (empty($competitionId)) {
                    return;
                }
                $seasonId = $node->attr('tournamentCalendarName');
                //Season id can be written in two ways:
                //1) a year (2015)
                //2) a mix of years (2015/2016)
                $seasonId = preg_match('/(\d)+/', $seasonId, $seasonIdYears);
                //Error or lack of matching
                if ($seasonId !== 1) {
                    return;
                }
                $collection->addId($this->generateProviderCompetitionSeasonId($competitionId, $seasonIdYears[0]));
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingTeamsFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_TEAM, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('playerCareer/person/membership')
            ->each(function (Crawler $node) use ($collection) {
                $id = $node->attr('opContestantId');
                //No id, skip it
                if (empty($id)) {
                    return;
                }
                //Opta does not provide the uid here, so we add the t manually
                $id = 't' . $id;
                $collection->addId($id);
            });
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingPeopleFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_PERSON, self::PROVIDER);
        $collection->addId($this->getPersonIdFromXml());
        return $collection;
    }

    /**
     * @return string
     */
    protected function getPersonIdFromXml(): string
    {
        if (!isset($this->personProviderId)) {
            $node = $this->getCrawledXmlDocument()->filterXPath('playerCareer/person')->first();
            //Opta does not provide the uid here, so we add the p manually
            $this->personProviderId = 'p' . $node->attr('opId');
        }
        return $this->personProviderId;
    }

    /**
     * @return string
     */
    protected function getPersonKnownFromXml(): string
    {
        if (!isset($this->personProviderKnown)) {
            $node = $this->getCrawledXmlDocument()->filterXPath('playerCareer/person')->first();
            $this->personProviderKnown = $node->attr('matchName');
        }
        return $this->personProviderKnown;
    }

    /**
     * @param string $type
     * @return string
     * @throws Exception
     */
    private final function getCompetitionType(string $type): string
    {
        switch (strtolower($type)) {
            case 'club':
                return CategoryInterface::COMPETITION_TYPE_CLUB;
            case 'national':
                return CategoryInterface::COMPETITION_TYPE_NATIONAL_TEAM;
            default:
                throw new Exception('Competition type not recognized');
        }
    }

    /**
     * @param string $type
     * @return bool
     */
    private final function getIsFriendly(string $type): bool
    {
        switch (strtolower($type)) {
            case 'no':
                return false;
            case 'yes':
            default:
                return true;
        }
    }

    /**
     * @param string $type
     * @return string
     */
    private final function getGender(string $type): string
    {
        switch (strtolower($type)) {
            case 'women':
                return 'FEMALE';
            case 'men':
            default:
                return 'MALE';
        }
    }

    /**
     * @return array
     */
    public function getStats(): array
    {
        return $this->stats;
    }
}