<?php

namespace App\Processor\Provider\Opta\Xml;

use AsResultados\OAMBundle\Model\Results\Broadcast\Broadcast;
use Exception;
use Symfony\Component\DomCrawler\Crawler;

class F13 extends F13m
{
    /**
     * @inheritDoc
     */
    public function run(): bool
    {
        if (!is_null($this->getBroadcast()) && ($this->getBroadcast()->isManual() === true)) {
            return false;
        }
        return parent::run();
    }

    /**
     * @param Crawler $node
     * @return Broadcast
     * @throws Exception
     */
    protected function createBroadcastFromXml(Crawler $node): Broadcast
    {
        $broadcast = parent::createBroadcastFromXml($node);
        $broadcast->setManual(false);
        return $broadcast;
    }
}