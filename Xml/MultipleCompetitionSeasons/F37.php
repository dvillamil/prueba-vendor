<?php

namespace App\Processor\Provider\Opta\Xml\MultipleCompetitionSeasons;

use App\Processor\Provider\Opta\Xml\F37 as F37Master;
use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\MappingInterface;
use AsResultados\OAMBundle\Exception\MissingItemException;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Results\PersonCompetitionSeason\PersonCompetitionSeason;
use Exception;
use Symfony\Component\DomCrawler\Crawler;

class F37 extends F37Master
{
    /**
     * @inheritDoc
     */
    protected function setProcessorVariablesFromMapping(): void
    {
        $mappingCollection = MappingCollection::getInstance();
        $this->setCompetitionSeasonId($mappingCollection->get(
            MappingInterface::ENTITY_COMPETITION_SEASON, self::PROVIDER, $this->getCompetitionSeasonFromXml()
        ));
        $this->setCompetitionSeasonsByStageIds(
            $mappingCollection->getMultiple(MappingInterface::ENTITY_STAGE, self::PROVIDER),
            $this->getClient()
        );
        $this->setMappingsOwn($this->getMappingsOwn());
    }

    /**
     * @inheritDoc
     */
    protected function getMappingsFromProviderData(): array
    {
        $mappings = parent::getMappingsFromProviderData();
        $mappings[] = $this->getMappingStagesFromXml();
        return $mappings;
    }

    /**
     * @return ProviderIdsCollection
     * @throws Exception
     */
    protected function getMappingStagesFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_STAGE, self::PROVIDER);
        $this->getCrawledXmlDocument($this->getFileSystemCalendarPath($this->getCompetitionFromXml(), $this->getSeasonFromXml()))
            ->filterXPath('SoccerFeed/SoccerDocument/MatchData/MatchInfo')
            ->each(function (Crawler $node) use ($collection) {
                $stageId = $this->generateProviderStageId(
                    $this->getCompetitionSeasonFromXml(),
                    $node->attr('RoundType'),
                    $node->attr('RoundNumber'));
                $collection->addId($stageId);
            });
        return $collection;
    }

    /**
     * @inheritDoc
     */
    protected function createRefereeCompetitionSeasonFromXmlNode(Crawler $node): PersonCompetitionSeason
    {
        $result = parent::createRefereeCompetitionSeasonFromXmlNode($node);
        //We do not know at which competitionSeason refers the stats or if they are mixed between more than one so we clean them
        $result->setStats(array());
        return $result;
    }

    /**
     * @inheritDoc
     */
    protected function getRefereeCompetitionSeasons(string $id): array
    {
        if (!isset($this->refereesInCompetitionSeasons)) {
            $mappingCollection = MappingCollection::getInstance();
            $result = array();
            $this->getCrawledXmlDocument($this->getFileSystemCalendarPath($this->getCompetitionFromXml(), $this->getSeasonFromXml()))
                ->filterXPath('SoccerFeed/SoccerDocument/MatchData/MatchInfo')
                ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                    $stageId = $this->generateProviderStageId(
                        $this->getCompetitionSeasonFromXml(),
                        $node->attr('RoundType'),
                        $node->attr('RoundNumber'));
                    if (!$mappingCollection->exists(
                        MappingInterface::ENTITY_STAGE,
                        self::PROVIDER,
                        $stageId)) {
                        //Do not have mapping for the stage, very strange at this point, but keep going (F1 will log this)
                        return;
                    }
                    $competitionSeasonId = $this->getCompetitionSeasonIdFromStageId(
                        $mappingCollection->get(
                            MappingInterface::ENTITY_STAGE,
                            self::PROVIDER,
                            $stageId)
                    );
                    $node->parents()->filterXPath('MatchData/MatchOfficials/MatchOfficial[@uID]')->each(function (Crawler $nodeReferee) use (&$result, $mappingCollection, $competitionSeasonId) {
                        if (!$mappingCollection->exists($mappingCollection::ENTITY_PERSON, self::PROVIDER, $nodeReferee->attr('uID'))) {
                            //We do not have this referee mapping, very strange but keep going
                            return;
                        }
                        $id = $mappingCollection->get($mappingCollection::ENTITY_PERSON, self::PROVIDER, $nodeReferee->attr('uID'));
                        if (!isset($result[$id][$competitionSeasonId])) {
                            $result[$id][$competitionSeasonId] = $competitionSeasonId;
                        }
                    });
                });
            if (empty($result)) {
                throw new Exception('There are not referees in calendar provider file');
            }
            $this->refereesInCompetitionSeasons = $result;
        }
        if (!isset($this->refereesInCompetitionSeasons[$id])) {
            throw new MissingItemException($id, 'refereesInCompetitionSeasons', 'can not associate referee id to any competitionSeason');
        }
        return $this->refereesInCompetitionSeasons[$id];
    }
}