<?php

namespace App\Processor\Provider\Opta\Xml;

use App\Processor\Provider\Opta\AbstractProcessor as AbstractProcessorOpta2;
use App\Processor\Provider\ProcessorXmlTrait;
use AsResultados\OAMBundle\Exception\MissingItemException;
use Exception;

abstract class AbstractProcessor extends AbstractProcessorOpta2
{
    use ProcessorXmlTrait;

    /**
     * @var string
     */
    protected $competitionProviderId;

    /**
     * @var string
     */
    protected $seasonProviderId;

    /**
     * @return string
     * @throws MissingItemException
     */
    protected function getCompetitionFromXml(): string
    {
        if (!isset($this->competitionProviderId)) {
            $node = $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument');
            if (is_null($node->attr('competition_id'))) {
                throw new MissingItemException('competition id', 'xml node', 'provider xml file');
            }
            $this->competitionProviderId = (string)$node->attr('competition_id');
        }
        return $this->competitionProviderId;
    }

    /**
     * @return string
     * @throws MissingItemException
     */
    protected function getSeasonFromXml(): string
    {
        if (!isset($this->seasonProviderId)) {
            $node = $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument');
            if (is_null($node->attr('season_id'))) {
                throw new MissingItemException('season_id', 'xml node', 'provider xml file');
            }
            $this->seasonProviderId = (string)$node->attr('season_id');
        }
        return $this->seasonProviderId;
    }

    /**
     * @return string
     * @throws Exception
     */
    protected function getCompetitionSeasonFromXml(): string
    {
        return $this->generateProviderCompetitionSeasonId(
            $this->getCompetitionFromXml(),
            $this->getSeasonFromXml()
        );
    }

    /**
     * @param string $competitionId
     * @param string $seasonId
     * @return string
     */
    protected function getFileSystemCalendarPath(string $competitionId, string $seasonId): string
    {
        return 'opta2/1/' . $competitionId . '/' . $seasonId . '/F1/srml-' . $competitionId . '-' . $seasonId . '-results.xml';
    }
}