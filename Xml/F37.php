<?php

namespace App\Processor\Provider\Opta\Xml;

use App\Processor\Provider\Traits\CompetitionSeasonsTrait;
use App\Utils\StringTrait;
use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\MappingInterface;
use AsResultados\OAMBundle\Api\Internal\Results\Category\CategoryInterface;
use AsResultados\OAMBundle\Api\Internal\Results\Person\Register as PersonRegister;
use AsResultados\OAMBundle\Api\Internal\Results\RequestsResourceItem;
use AsResultados\OAMBundle\Exception\DatabaseInconsistencyException;
use AsResultados\OAMBundle\Exception\MissingItemException;
use AsResultados\OAMBundle\Model\Results\CompetitionSeason\CompetitionSeason;
use AsResultados\OAMBundle\Model\Results\Person\Embed\Name;
use AsResultados\OAMBundle\Model\Results\Person\Person;
use AsResultados\OAMBundle\Model\Results\PersonCompetitionSeason\PersonCompetitionSeason;
use AsResultados\OAMBundle\Model\Results\PersonCompetitionSeason\Embed\Stat as PersonStat;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Collection\Collection;
use AsResultados\OAMBundle\Api\Internal\Results\PersonCompetitionSeason\Request as PersonCompetitionSeasonRequest;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use AsResultados\OAMBundle\Exception\EmptyItemException;
use AsResultados\OAMBundle\Exception\MappingException;
use App\Processor\Provider\Traits\CompetitionSeasonIdTrait;
use App\Utils\Date;
use DateTimeZone;
use Exception;
use Symfony\Component\DomCrawler\Crawler;
use AsResultados\OAMBundle\Api\Internal\Results\PersonCompetitionSeason\Register as PersonSeasonRegister;

class F37 extends AbstractProcessor
{
    use CompetitionSeasonIdTrait;
    use CompetitionSeasonsTrait;
    use StringTrait;

    /**
     * Associates referees ids with the competitionSeasons in which they played
     * @var string[]
     */
    protected $refereesInCompetitionSeasons;

    /**
     * @inheritDoc
     */
    protected function setProcessorVariablesFromMapping(): void
    {
        $mappingCollection = MappingCollection::getInstance();
        $this->setCompetitionSeasonId(
            $mappingCollection->get(
                MappingInterface::ENTITY_COMPETITION_SEASON, self::PROVIDER, $this->getCompetitionSeasonFromXml()
            )
        );
        $this->setCompetitionSeasons([$this->getCompetitionSeasonId()], $this->getClient());
        $this->setMappingsOwn($this->getMappingsOwn());
    }

    /**
     * @inheritDoc
     */
    public function run(): bool
    {
        //Get people
        $people = $this->getRefereesFromXml();
        $personRegister = PersonRegister::getInstance($this->getClient());
        //Insert people
        try {
            $personRegister->postWithMapping(
                $people->getAllUnRegistered(), $people->getUnRegisteredIds(), MappingInterface::ENTITY_PERSON, self::PROVIDER
            );
            $opInsertPeople = true;
            //Add new people to collection
            $people->removeAllUnRegistered();
            $people->addMultipleRegistered($personRegister->getLastInsertedItems());
        } catch (DatabaseInconsistencyException $e) {
            $this->getLogger()->emergency('Can not insert people: ' . $e->getMessage());
            throw $e;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not insert people: ' . $e->getMessage());
        }
        //Get peopleCompetitionSeason
        $peopleCompetitionSeason = $this->getRefereesCompetitionSeasonFromXml();
        $personSeasonRegister = PersonSeasonRegister::getInstance($this->getClient());
        //Update peopleCompetitionSeason
        try {
            $personSeasonRegister->patch($peopleCompetitionSeason->getAllRegistered());
            $opUpdatePeopleCompetitionSeason = true;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update peopleCompetitionSeason: ' . $e->getMessage());
        }
        //Insert peopleCompetitionSeason
        try {
            $personSeasonRegister->post($peopleCompetitionSeason->getAllUnRegistered());
            $opInsertPeopleCompetitionSeason = true;
            //Add new peopleCompetitionSeason to collection
            $peopleCompetitionSeason->removeAllUnRegistered();
            $peopleCompetitionSeason->addMultipleRegistered($personSeasonRegister->getLastInsertedItems());
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not insert peopleCompetitionSeason: ' . $e->getMessage());
        }
        //Update peopleCompetitionSeason inside people
        $peopleCompetitionSeason->eachRegistered(function (PersonCompetitionSeason $personCompetitionSeason) use (&$people) {
            if ($people->existsRegistered($personCompetitionSeason->getPerson()->getId())) {
                $tmp = $people->getRegistered($personCompetitionSeason->getPerson()->getId());
                if ($tmp instanceof Person) {
                    $competitionsSeasons = $tmp->getCompetitionsSeasons();
                    if (!is_array($competitionsSeasons)) {
                        $competitionsSeasons = array();
                    }
                    $competitionsSeasons[] = $personCompetitionSeason->getId();
                    $tmp->setCompetitionsSeasons($competitionsSeasons);
                }
            }
        });
        //Update people
        try {
            $personRegister->patch($people->getAllRegistered());
            $opUpdatePeople = true;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update people: ' . $e->getMessage());
        }
        //Dispatch events
        if (isset($opInsertPeople) || isset($opInsertPeopleCompetitionSeason) ||
            isset($opUpdatePeople) || isset($opUpdatePeopleCompetitionSeason)) {
            $this->dispatchEvents();
        }
        return true;
    }

    private function dispatchEvents(): void
    {
    }

    /**
     * @inheritDoc
     */
    protected function getMappingsFromProviderData(): array
    {
        $mappings = array();
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_COMPETITION_SEASON, self::PROVIDER);
        $collection->addId($this->getCompetitionSeasonFromXml());
        $mappings[] = $collection;
        $mappings[] = $this->getMappingRefereesFromXml();
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_STAT_REFEREE, self::PROVIDER, true);
        return $mappings;
    }

    /**
     * @throws Exception
     */
    protected function getMappingsOwn(): array
    {
        $requests = array();
        $mappingCollection = MappingCollection::getInstance();
        $request = new PersonCompetitionSeasonRequest();
        foreach ($this->getCompetitionSeasons() as $competitionSeason) {
            $requests[] = new RequestsResourceItem(
                $request->getByPeopleAndCompetitionSeason(
                    $mappingCollection->getMultiple(MappingInterface::ENTITY_PERSON, self::PROVIDER),
                    $competitionSeason->getId()
                ),
                $request->getResource()
            );
        }
        return $requests;
    }

    /**
     * @return Collection
     * @throws Exception
     */
    protected function getRefereesFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(Person::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/Referee')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                $id = $node->attr('uID');
                if (empty($id)) {
                    //No id, skip it
                    return;
                }
                $item = $this->createRefereeFromXmlNode($node);
                if ($mappingCollection->exists($mappingCollection::ENTITY_PERSON, self::PROVIDER, $id)) {
                    $item->setId($mappingCollection->get($mappingCollection::ENTITY_PERSON, self::PROVIDER, $id));
                    //Set slug to null because it is already inserted
                    $item->setSlug(null);
                    $result->addRegistered($item);
                } else {
                    $result->addUnRegistered($item, $id);
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return Person
     * @throws MappingException
     */
    protected function createRefereeFromXmlNode(Crawler $node): Person
    {
        $mappingCollection = MappingCollection::getInstance();
        $person = new Person();
        $name = new Name();
        $data = $node->filterXPath('Referee/Stat[@Type="first_name"]');
        if ($data->count() > 0) {
            $name->setFirst($data->text());
        }
        $data = $node->filterXPath('Referee/Stat[@Type="last_name"]');
        if ($data->count() > 0) {
            $name->setLast($data->text());
            $name->setKnown($data->text());
        }
        $person->setName($name);
        $person->setSlug($this->normalizeString($name->getFirst() . ' ' . $name->getLast()));
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Country & first nationalization
        /// First nationality is the birth country of the referee if it exists and country the nationality.
        /// Otherwise, if first nationality does not exists, country represents both values
        $data = $node->filterXPath('Referee/Stat[@Type="country"]');
        if ($data->count() > 0 && !is_null($this->checkValue($data->text()))) {
            if ($mappingCollection->exists($mappingCollection::ENTITY_COUNTRY, self::PROVIDER, $data->text())) {
                $country = $mappingCollection->get(
                    $mappingCollection::ENTITY_COUNTRY,
                    self::PROVIDER,
                    $data->text()
                );
                $person->setNationalizationById($country);
                $person->setCountryById($country);
            }
        }
        $data = $node->filterXPath('Referee/Stat[@Type="first_nationality"]');
        if ($data->count() > 0 && !is_null($this->checkValue($data->text()))) {
            if ($mappingCollection->exists($mappingCollection::ENTITY_COUNTRY, self::PROVIDER, $data->text())) {
                $person->setCountryById($mappingCollection->get(
                    $mappingCollection::ENTITY_COUNTRY,
                    self::PROVIDER,
                    $data->text()
                ));
            }
        }
        /// Country & first nationalization
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        $data = $node->filterXPath('Referee/Stat[@Type="country"]');
        if ($data->count() > 0) {
            if ($mappingCollection->exists($mappingCollection::ENTITY_COUNTRY, self::PROVIDER, $data->text())) {
                $person->setCountryById($mappingCollection->get(
                    $mappingCollection::ENTITY_COUNTRY,
                    self::PROVIDER,
                    $data->text()
                ));
            }
        }
        $data = $node->filterXPath('Referee/Stat[@Type="birth_date"]');
        if ($data->count() > 0) {
            try {
                $date = new Date($data->text(), new DateTimeZone(self::DATETIME_ZONE));
                $person->setBirthDate($date->format($date::FORMAT_Ymd));
            } catch (Exception $e) {
                //Do nothing, date will not be set
            }
        }
        return $person;
    }

    /**
     * @return Collection
     * @throws Exception
     */
    protected function getRefereesCompetitionSeasonFromXml(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(PersonCompetitionSeason::class);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/Referee')
            ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                try {
                    $person = $this->createRefereeCompetitionSeasonFromXmlNode($node);
                    $competitionSeasons = $this->getRefereeCompetitionSeasons($person->getPerson()->getId());
                    foreach ($competitionSeasons as $competitionSeason) {
                        $item = clone($person);
                        $competitionSeasonObject = new CompetitionSeason();
                        $competitionSeasonObject->setId($competitionSeason);
                        $item->setCompetitionSeason($competitionSeasonObject);
                        if ($mappingCollection->existsOwn(PersonCompetitionSeason::class, $item->getUniqueIdByRelations())) {
                            $item->setId($mappingCollection->getOwn(PersonCompetitionSeason::class, $item->getUniqueIdByRelations()));
                            $result->addRegistered($item);
                        } else {
                            $result->addUnRegistered($item, $item->getUniqueIdByRelations());
                        }
                    }
                } catch (MissingItemException $e) {
                    //Skip item and keep going
                } catch (MappingException $e) {
                    //Mapping error, skip item and keep going
                }
            });
        return $result;
    }

    /**
     * @param Crawler $node
     * @return PersonCompetitionSeason
     * @throws MappingException
     */
    protected function createRefereeCompetitionSeasonFromXmlNode(Crawler $node): PersonCompetitionSeason
    {
        $mappingCollection = MappingCollection::getInstance();
        $personCompetitionSeason = new PersonCompetitionSeason();
        $personCompetitionSeason->setPersonById($mappingCollection->get(
            $mappingCollection::ENTITY_PERSON,
            self::PROVIDER,
            $node->attr('uID')
        ));
        $personCompetitionSeason->setTypeById(CategoryInterface::PERSON_TYPE_REFEREE);
        $personCompetitionSeason->setLoan(false);
        //If inside the xml it is active
        $personCompetitionSeason->setActive(true);
        $data = $node->filterXPath('Referee/Name');
        if ($data->count() > 0) {
            $personCompetitionSeason->setName($data->text());
        }
        $stats = array();
        $node->filterXPath('Referee/Stat')
            ->each(function (Crawler $node) use (&$stats, $mappingCollection) {
                if (!$mappingCollection->exists(
                    $mappingCollection::ENTITY_STAT_REFEREE,
                    self::PROVIDER,
                    $node->attr('Type')
                )) {
                    //We do not have mapping for that stat, keep going
                    return;
                }
                $stat = new PersonStat();
                $stat->setStatById($mappingCollection->get(
                    $mappingCollection::ENTITY_STAT_REFEREE,
                    self::PROVIDER,
                    $node->attr('Type')
                ));
                $stat->setValue($node->text());
                $stats[] = $stat;
            });
        $personCompetitionSeason->setStats($stats);
        return $personCompetitionSeason;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingRefereesFromXml(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_PERSON, self::PROVIDER);
        $this->getCrawledXmlDocument()
            ->filterXPath('SoccerFeed/SoccerDocument/Referee[@uID]')
            ->each(function (Crawler $node) use ($collection) {
                $collection->addId($node->attr('uID'));
            });
        return $collection;
    }

    /**
     * @return string
     * @throws MissingItemException
     */
    protected function getCompetitionFromXml(): string
    {
        if (!isset($this->competitionProviderId)) {
            $node = $this->getCrawledXmlDocument()->filterXPath('SoccerFeed/SoccerDocument');
            if (is_null($node->attr('comp_id'))) {
                throw new MissingItemException('comp_id for competition id', 'xml node', 'provider xml file');
            }
            $this->competitionProviderId = (string)$node->attr('comp_id');
        }
        return $this->competitionProviderId;
    }

    /**
     * @param string $id
     * @return string[]
     * @throws MissingItemException
     * @throws Exception
     */
    protected function getRefereeCompetitionSeasons(string $id): array
    {
        if (!isset($this->refereesInCompetitionSeasons)) {
            $mappingCollection = MappingCollection::getInstance();
            $result = array();
            $this->getCrawledXmlDocument()
                ->filterXPath('SoccerFeed/SoccerDocument/Referee[@uID]')
                ->each(function (Crawler $node) use (&$result, $mappingCollection) {
                    if (!$mappingCollection->exists($mappingCollection::ENTITY_PERSON, self::PROVIDER, $node->attr('uID'))) {
                        //We do not have that mapping, very strange but keep going
                        return;
                    }
                    $id = $mappingCollection->get($mappingCollection::ENTITY_PERSON, self::PROVIDER, $node->attr('uID'));
                    if (!isset($result[$id][$this->getCompetitionSeasonId()])) {
                        $result[$id][$this->getCompetitionSeasonId()] = $this->getCompetitionSeasonId();
                    }
                });
            $this->refereesInCompetitionSeasons = $result;
        }
        if (!isset($this->refereesInCompetitionSeasons[$id])) {
            throw new MissingItemException($id, 'refereesInCompetitionSeasons', 'can not associate referee id to any competitionSeason');
        }
        return $this->refereesInCompetitionSeasons[$id];
    }
}